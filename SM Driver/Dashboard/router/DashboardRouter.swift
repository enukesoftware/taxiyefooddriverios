//
//  DashboardRouter.swift
//  SM Driver
//
//  Created by mac-2 on 06/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit

class DashoardRouter : PresenterToRouter {
 
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    static func createModule() -> DashboardViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        
        let presenter: DashboardPresenter = DashboardPresenter()
        let interactor: DashboardInteractor = DashboardInteractor()
        let router:DashoardRouter = DashoardRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    func moveToNext() {
        // Move to next
        print("Move to next")
        let dashboard = LoginRouter.createModule()
        let navigationController = UINavigationController(rootViewController: dashboard)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController

    }
}
