//
//  OrderListResponseEntity.swift
//  SM Driver
//
//  Created by mac-2 on 26/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
// MARK: - OrderListResponse
struct OrderListResponse: Codable {
    let success : Bool?
    let data: [OrderList]?
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        data = try values.decodeIfPresent([OrderList].self, forKey: .data)
    }
}

// MARK: - Datum
struct OrderList: Codable {
    let ship_json : ShipJSON?
    let id : String?
    let driver_id : String?
    let date : String?
    let time : String?
    let asap : String?
    let amount : String?
    let discount : String?
    let order_type : String?
    let shipping_charge : String?
    let order_number : String?
    let status : String?
    let remark : String?
    let created_at : String?
    let ship_add1 : String?
    let ship_add2 : String?
    let ship_city : String?
    let ship_zip : String?
    let ship_mobile : String?
    let ship_lat : String?
    let ship_long : String?
    let first_name : String?
    let last_name : String?
    let contact_number : String?
    let coupon_code : String?
    let coupon_value : String?
    let restaurant_name : String?
    let restaurant_longitude : String?
    let restaurant_latitude : String?
    let restaurant_rating : String?
    let restaurant_floor : String?
    let restaurant_street : String?
    let restaurant_company : String?
    let restaurant_area : String?
    let restaurant_city : String?
    var isseen:Bool?
    let profile_image:String?
    
    enum CodingKeys: String, CodingKey {
        
        case ship_json = "ship_json"
        case id = "id"
        case driver_id = "driver_id"
        case date = "date"
        case time = "time"
        case asap = "asap"
        case amount = "amount"
        case discount = "discount"
        case order_type = "order_type"
        case shipping_charge = "shipping_charge"
        case order_number = "order_number"
        case status = "status"
        case remark = "remark"
        case created_at = "created_at"
        case ship_add1 = "ship_add1"
        case ship_add2 = "ship_add2"
        case ship_city = "ship_city"
        case ship_zip = "ship_zip"
        case ship_mobile = "ship_mobile"
        case ship_lat = "ship_lat"
        case ship_long = "ship_long"
        case first_name = "first_name"
        case last_name = "last_name"
        case contact_number = "contact_number"
        case coupon_code = "coupon_code"
        case coupon_value = "coupon_value"
        case restaurant_name = "restaurant_name"
        case restaurant_longitude = "restaurant_longitude"
        case restaurant_latitude = "restaurant_latitude"
        case restaurant_rating = "restaurant_rating"
        case restaurant_floor = "restaurant_floor"
        case restaurant_street = "restaurant_street"
        case restaurant_company = "restaurant_company"
        case restaurant_area = "restaurant_area"
        case restaurant_city = "restaurant_city2"
        case profile_image = "profile_image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        ship_json = try values.decodeIfPresent(ShipJSON.self, forKey: .ship_json)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        driver_id = try values.decodeIfPresent(String.self, forKey: .driver_id)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        asap = try values.decodeIfPresent(String.self, forKey: .asap)
        amount = try values.decodeIfPresent(String.self, forKey: .amount)
        discount = try values.decodeIfPresent(String.self, forKey: .discount)
        order_type = try values.decodeIfPresent(String.self, forKey: .order_type)
        shipping_charge = try values.decodeIfPresent(String.self, forKey: .shipping_charge)
        order_number = try values.decodeIfPresent(String.self, forKey: .order_number)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        remark = try values.decodeIfPresent(String.self, forKey: .remark)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        ship_add1 = try values.decodeIfPresent(String.self, forKey: .ship_add1)
        ship_add2 = try values.decodeIfPresent(String.self, forKey: .ship_add2)
        ship_city = try values.decodeIfPresent(String.self, forKey: .ship_city)
        ship_zip = try values.decodeIfPresent(String.self, forKey: .ship_zip)
        ship_mobile = try values.decodeIfPresent(String.self, forKey: .ship_mobile)
        ship_lat = try values.decodeIfPresent(String.self, forKey: .ship_lat)
        ship_long = try values.decodeIfPresent(String.self, forKey: .ship_long)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        contact_number = try values.decodeIfPresent(String.self, forKey: .contact_number)
        coupon_code = try values.decodeIfPresent(String.self, forKey: .coupon_code)
        coupon_value = try values.decodeIfPresent(String.self, forKey: .coupon_value)
        restaurant_name = try values.decodeIfPresent(String.self, forKey: .restaurant_name)
        restaurant_longitude = try values.decodeIfPresent(String.self, forKey: .restaurant_longitude)
        restaurant_latitude = try values.decodeIfPresent(String.self, forKey: .restaurant_latitude)
        restaurant_rating = try values.decodeIfPresent(String.self, forKey: .restaurant_rating)
        restaurant_floor = try values.decodeIfPresent(String.self, forKey: .restaurant_floor)
        restaurant_street = try values.decodeIfPresent(String.self, forKey: .restaurant_street)
        restaurant_company = try values.decodeIfPresent(String.self, forKey: .restaurant_company)
        restaurant_area = try values.decodeIfPresent(String.self, forKey: .restaurant_area)
        restaurant_city = try values.decodeIfPresent(String.self, forKey: .restaurant_city)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
    }
}

// MARK: - ShipJSON


// MARK: - AddressJSON

