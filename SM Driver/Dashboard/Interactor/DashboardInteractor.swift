//
//  DashboardInteractor.swift
//  SM Driver
//
//  Created by mac-2 on 04/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation


class DashboardInteractor : NSObject, PresenterToInteractor , CLLocationManagerDelegate  {
    
    var presenter : InteractorToPresnter?
    
    var locationManager:CLLocationManager!

    var isMontioring = false;
    
    func statusActive() {
        updateDriverStatus(status: "1")
    }
    
    func statusDeactive() {
        updateDriverStatus(status: "0")
    }
    
    
    func updateDriverStatus(status:String){
        if(!Connectivity.isConnectedToInternet){
            presenter?.stausUpdateFail(error: "Internet is not connected")
            return
        }
        
        let inputDic:[String:Any] = ["driver_id":UserDefaultUtils.getUserId(),"status":status]
        
        ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.UPDATE_STATUS,headerString: "",  onCompletion: {
            (json: JSON, err: NSError?) in
            if err != nil{
                self.presenter?.stausUpdateFail(error: err.debugDescription)
            }
            else{
//                print("Json:",json.stringValue)
                print("json:",json)
                if json["success"].boolValue == true {
                    self.presenter?.stausUpdateSucess()
                    if status == "0"{
                        UserDefaultUtils.setDriverActiveStatus(status: false)
                    }
                    else{
                        UserDefaultUtils.setDriverActiveStatus(status: true)
                    }
                }
                else{
                    let message = json["message"].stringValue
                    self.presenter?.stausUpdateFail(error: message)
                }
            }
        })
    }
    
    func startUpdateLocation() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
        }
        else{
            self.presenter?.stausUpdateFail(error: "Please Enable location service")
        }
        switch(CLLocationManager.authorizationStatus()) {
        case .notDetermined, .restricted, .denied:
            print("No access")
            
        case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
//            if (!isMontioring){
                locationManager = CLLocationManager()
                locationManager.delegate = self
                 locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startMonitoringSignificantLocationChanges()
//            }
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         let userLocation:CLLocation = locations[0] as CLLocation
        
        let latitude =  String(format: "%.6f", userLocation.coordinate.latitude)
        let longitude = String(format: "%.6f", userLocation.coordinate.longitude)
        print("latitude = ",latitude)
        print("longitude = ",longitude)
//        print("user latitude = \(userLocation.coordinate.latitude)")
//        print("user longitude = \(userLocation.coordinate.longitude)")
        locationManager.stopUpdatingLocation()
        isMontioring = true
        
        if UserDefaultUtils.getDriverActiveStatus()
        {
//           makeServerRequestForOrder(latitude: "28.4576818", longitude: "77.0449214")
//        makeServerRequestForOrder(latitude: "\(userLocation.coordinate.latitude)", longitude: "\(userLocation.coordinate.longitude)")
            makeServerRequestForOrder(latitude: "\(latitude)", longitude: "\(longitude)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted")
            presenter?.stausUpdateFail(error: "Application can't access your location.")
        case .denied:
            print("User denied access to location")
             presenter?.stausUpdateFail(error: "User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways:
            if (!isMontioring){
                locationManager.delegate = self
                 locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startMonitoringSignificantLocationChanges()
            }
        case .authorizedWhenInUse:
            print("Location status is OK")
            if (!isMontioring){
                locationManager.delegate = self
                 locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startMonitoringSignificantLocationChanges()
                
            }
        }
    }
    
    func makeServerRequestForOrder(latitude:String,longitude:String){
       // print("log")
        if(!Connectivity.isConnectedToInternet){
            presenter?.stausUpdateFail(error: "Internet is not connected")
            return
        }
        if !UserDefaultUtils.getDriverActiveStatus(){
            return
        }
        
        
        let inputDic:[String:Any] = ["driver_id":UserDefaultUtils.getUserId(),"status":"2","lat":latitude,"long":longitude]
        
       // print(inputDic)
        ApiHandler.sharedInstance.makeHTTPPostRequestWithData(path: Constants.BASE_URL + Constants.API_VERSION+Constants.GET_ORDERS, body: inputDic as [String : AnyObject], headerString: "", onCompletion: { data , err in   
            
            // onCompletion(json as JSON, err)
            self.isMontioring = false
            
            if err != nil{
                self.presenter?.stausUpdateFail(error: err.debugDescription)
            }
            else{
//                let string1 = String(data: data!, encoding: String.Encoding.utf8) ?? "Data could not be printed"
               // print(string1)
                do {
                    let decoder = JSONDecoder()
                let orders = try decoder.decode(OrderListResponse.self, from: data!)
                    if let orderList = orders.data{
                        print("Order :",orderList)
                     self.isMontioring = false
                   self.presenter?.sendOrderListData(orderList: orderList)
                }
                }
                catch let error  {
                    print(error.localizedDescription)
                    
                }
            }
        })
        
    }
    
    func accpectOrder(order: OrderList) {
        
        if(!Connectivity.isConnectedToInternet){
            presenter?.stausUpdateFail(error: "Internet is not connected")
            return
        }
        
        let inputDic:[String:Any] = ["driver_id":UserDefaultUtils.getUserId(),"status":"3","order_id":order.id,"remark":""]
        
        ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.UPDATE_ORDER,headerString: "",  onCompletion: {
            (json: JSON, err: NSError?) in
            if err != nil{
                self.presenter?.stausUpdateFail(error: err.debugDescription)
            }
            else{
                print("Accep =",json)
                if json["success"].boolValue == true {
                    self.presenter?.accpectOrderStatus(order: order)
                   
                }
                else{
                    let message = json["message"].stringValue
                    self.presenter?.stausUpdateFail(error: message)
                }
            }
        })
    }
    
    //
    func callapiUpdateOrder(reason:String?,status:String)
     {
        
        if(!Connectivity.isConnectedToInternet){
            presenter?.stausUpdateFail(error: "Internet is not connected")
            return
        }
        
        let inputDic:[String:Any] = ["driver_id":UserDefaultUtils.getUserId(),"status":status,"order_id":UserDefaultUtils.getOrderid(),"remark":reason!]
        
        ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.UPDATE_ORDER,headerString: "",  onCompletion: {
            (json: JSON, err: NSError?) in
            if err != nil{
                self.presenter?.stausUpdateFail(error: err.debugDescription)
            }
            else{
                print(json.stringValue)
                if json["success"].boolValue == true {
                    UserDefaultUtils.setCurrrentOrderStatus(status: status)
                    
                    if status == "4"
                    {
                        self.presenter?.updateStausOfOrder(status: "start tracking location")

                    }else if status == "5"
                    {
                        self.presenter?.updateStausOfOrder(status: "delivered")
                        
                    }
                    else
                    {
                    self.presenter?.updateStausOfOrder(status: json["message"].stringValue)
                    }
                    
                }
                else{
                    let message = json["message"].stringValue
                    self.presenter?.stausUpdateFail(error: message)
                }
            }
        })
        
        
     }
    
    
    }
