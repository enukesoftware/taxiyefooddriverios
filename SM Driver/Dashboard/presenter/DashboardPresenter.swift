//
//  DashboardPresenter.swift
//  SM Driver
//
//  Created by mac-2 on 04/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class DashboardPresenter : ViewToPresnter,InteractorToPresnter {
    
   
    var interactor: PresenterToInteractor?
    
    var view: PresenterToView?
    
    var router : PresenterToRouter?
    
    var getOrderList = Timer()
    var current:OrderList?
    
    
    
    // call to stop timer
    
    func stopTimer()
    {
        self.getOrderList.invalidate()
    }
    
    
    
    // get reason from view and call to interactor to update order

    func updateOrder(reason:String?,status:String)
    {
        interactor?.callapiUpdateOrder(reason: reason,status:status)
    }
    
   
    
    
    // get response from interactor for order update and notify view 
    func updateStausOfOrder(status:String?)
    {
        view?.statusUpdateOrder(response: status!)
        
    }
    
    
    func statusBtnClick() {
        if UserDefaultUtils.getDriverActiveStatus() {
            interactor?.statusDeactive()
        }
        else{
            interactor?.statusActive()
        }
    }
    
    func stausUpdateSucess() {
        view?.statusUpdated()
    }
    
    func stausUpdateFail(error: String) {
        view?.statusUpdateFail(error: error)
    }
    
    func moveToLogin() {
        UserDefaultUtils.setLoggedIn(isLogin: false)
        UserDefaultUtils.setUserId(userId: "0")
        router?.moveToNext()
    }
    
    func checkLocationStatus() {
        if UserDefaultUtils.getDriverActiveStatus(){
            self.getOrderList.invalidate()
            updateLocation()
            startOrderTimer()
        }
        else{
            self.getOrderList.invalidate()
        }
    }
    
    func sendOrderListData(orderList: [OrderList]) {
        view?.updateOrderTable(orderList: orderList)
    }
    
    func startOrderTimer(){
         self.getOrderList = Timer.scheduledTimer(timeInterval: TimeInterval(60), target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
    }
    
    @objc func updateLocation(){
        interactor?.startUpdateLocation()
    }
    
    func accpectOrderClick(order: OrderList) {
        interactor?.accpectOrder(order: order)
    }
    func accpectOrderStatus(order:OrderList) {
        view?.accpectOrderSucess(order: order)
    }
}

extension DashboardViewController : UITableViewDelegate, UITableViewDataSource {
    
    func getorderlistobject(inoutarray: inout[OrderList],indexpath:Int) ->OrderList
    {
        
        return  inoutarray[indexpath]
        
    }
    
    func change(inoutarray: inout[OrderList],indexpath:Int)
    {
        
       inoutarray[indexpath].isseen = true
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaultUtils.getOrderDictionary() != nil
        {
            return 4
        }
        else
        {
        return (self.orderList.count)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UserDefaultUtils.getOrderDictionary() != nil
        {
            return UITableView.automaticDimension
//            var height = 300
//
//            switch(indexPath.row)
//            {
//            case 0:
//                height =  116
//
//            break
//            case 1:
//                height =  96
//
//                break
//
//            case 2:
//                height =  96
//
//                break
//
//            default:
//               height = 300
//           }
//            return CGFloat(height)
        }
        else
        {
        return 300
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if UserDefaultUtils.getOrderDictionary() != nil
        {
           
            
           switch(indexPath.row)
           {
           case 0:
           let cell = tableView.dequeueReusableCell(withIdentifier: "OrderIdTableViewCell", for: indexPath) as! OrderIdTableViewCell
            if let orderdictionary = UserDefaultUtils.getOrderDictionary()
            {
                cell.setValueToLabel(orderid: orderdictionary[OrderDictionaryenum.ordernumber.instance] as? String, amount: orderdictionary[OrderDictionaryenum.orderAmount.instance] as? String)
            }
           
           cell.cancelButton .addTarget(self, action: (#selector(cancelOrder)), for: .touchUpInside)
           
           return cell
            
            
           case 1:
           let  cell = tableView.dequeueReusableCell(withIdentifier: "OrderAddressTableViewCell", for: indexPath) as! OrderAddressTableViewCell
           
           if let orderdictionary = UserDefaultUtils.getOrderDictionary()
           {
            cell.setValueInLabel(type: "pickup", locationName: orderdictionary[OrderDictionaryenum.orderPickupAddress.instance] as? String)
            if let resturantName = orderdictionary[OrderDictionaryenum.resturantName.instance]
            {
                cell.locationNamelabel.text = resturantName as? String
            }
            
           }
           
           cell.mapButton.tag = 1
           cell.mapButton.addTarget(self, action: (#selector(openGooglemaps(sender:))), for: .touchUpInside)
           
           return cell
            
            
           case 2:
           let cell = tableView.dequeueReusableCell(withIdentifier: "OrderAddressTableViewCell", for: indexPath) as! OrderAddressTableViewCell
           if let orderdictionary = UserDefaultUtils.getOrderDictionary()
           {
            cell.setValueInLabel(type: "", locationName: orderdictionary[OrderDictionaryenum.orderDeliveryAddress.instance] as? String)
            
            if let dropName = orderdictionary[OrderDictionaryenum.deliveryName.instance]
            {
                cell.locationNamelabel.text = dropName as? String
            }
           }
           cell.mapButton.tag = 2
           cell.mapButton.addTarget(self, action: (#selector(openGooglemaps(sender:))), for: .touchUpInside)
           return cell
            
            
            case 3:
                
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerInfomationTableViewCell", for: indexPath) as! CustomerInfomationTableViewCell

            if let orderdictionary = UserDefaultUtils.getOrderDictionary()
            {
                if let urlString = orderdictionary[OrderDictionaryenum.CustomerImg.instance]{
                    var escapedString = (urlString as AnyObject).addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                        var url:NSURL = NSURL(string: escapedString as! String)!
                        var data = NSData(contentsOf: url as URL)
                        cell.customerImg.image = UIImage(data: data! as Data)
                }
                if let name = orderdictionary[OrderDictionaryenum.orderUserName.instance]{
                    cell.customerName.text = name as? String
                 }
                if let contact = orderdictionary[OrderDictionaryenum.orderUserContact.instance]{
                   cell.customerMobileNum.text = contact as? String
                    cell.callCustomerButton.accessibilityHint = contact as! String
                }
            
            }
            
           

            cell.callCustomerButton.addTarget(self, action: (#selector(callCustomer(sender:))), for: .touchUpInside)
            
             return cell
            
           default:
           return UITableViewCell()
            }
            
          
        }
        else
        {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderListCell", for: indexPath) as! OrderListCell
        
        cell.selectionStyle = .none
        
        cell.accpectOrder = self
        
        
            var current = orderList[indexPath.row]
            //let current = getorderlistobject(inoutarray: &orderList, indexpath: indexPath.row )
        
            
        if let id = orderList[indexPath.row].isseen
        {
            print(current.id!,"id",current.isseen!,"isseen");
            }
            
            
           // self.change(inoutarray: &orderList, indexpath: indexPath.row)
            
    
        cell.order = current
        
        cell.pickup_name.text = current.restaurant_name != nil ? current.restaurant_name : ""
        

        
        var pickupAddress:String = ""
         pickupAddress = current.restaurant_floor == "" ? "": current.restaurant_floor ?? " " + ", "
        
        if let restaurantStreet = current.restaurant_street{
            if restaurantStreet != ""{
                pickupAddress = pickupAddress + restaurantStreet + ", "
            }
        }
        
        if let restaurantArea = current.restaurant_area{
            if restaurantArea != ""{
                pickupAddress = pickupAddress + restaurantArea + ", "
            }
        }
        
        if let restaurantCity = current.restaurant_city{
            if restaurantCity != ""{
                pickupAddress = pickupAddress + restaurantCity
            }
        }
        
        cell.pickup_address.text  = pickupAddress
        
        cell.deliverd_name.text = (current.first_name == "" ? "": current.first_name ?? " ")+(current.last_name == "" ? "":current.last_name ?? " ")
        
        var deliverAddress:String = ""
        if let floor = current.ship_json?.address_json?.floor_unit{
            if floor != ""{
                deliverAddress = deliverAddress+floor+", "
            }
        }
        
        if let houseNo = current.ship_json?.address_json?.house_no {
        if houseNo != ""{
            deliverAddress = deliverAddress + houseNo + ", "
        }
        }
        
        if let street = current.ship_json?.address_json?.street {
        if street != "" {
            deliverAddress = deliverAddress + street + ", "
        }
        }
        
        if let areaname = current.ship_json?.address_json?.area_name {
        if areaname != "" {
            deliverAddress = deliverAddress + areaname + ", "
        }
        }
        
        if let city = current.ship_json?.address_json?.city{
        if city != "" {
            deliverAddress = deliverAddress + city
        }
       }
        cell.deliver_address.text = deliverAddress
        
        cell.indexOfCell = indexPath.row

//        if let timetoknow = cell.timeLeft
//        {
//            print("timetoknow",timetoknow)
//              cell.timeLeft = timetoknow
//        }
//         else
//        {
//             cell.timeLeft  =  30
//
//
//            }
            
            // Count Down timer
            cell.timeLeft  =  getTimerValueForCell(orderid: current.id!)
            cell.id = current.id
            
            cell.timer = Timer.scheduledTimer(timeInterval: 1, target: cell,   selector: (#selector(cell.updateTimer)), userInfo: nil, repeats: true)
        
        return cell
    }
    }
    
    
    func getTimerValueForCell(orderid:String) -> Int
    {
        if (self.currentTimeleftDict[orderid] != nil)
        {
            return self.currentTimeleftDict[orderid]!
        }
        else
        {
            return 30
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
         if let orderlistcell = cell as? OrderListCell
         {
            orderlistcell.timer.invalidate()
            let key = orderlistcell.id as! String
            self.currentTimeleftDict[key] = orderlistcell.timeLeft
            
        }
    }
    
    
    
    
    
    
    
}
