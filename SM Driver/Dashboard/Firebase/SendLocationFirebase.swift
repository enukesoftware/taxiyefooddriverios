//
//  SendLocationFirebase.swift
//  SM Driver
//
//  Created by mac-2 on 07/08/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase

class SendLocationFirebase: NSObject,CLLocationManagerDelegate {
    
    let deliverLatitude: Double!
    let deliverLongitude: Double!
    
    var orderStatus:String!
    let pickUpLatitude: Double!
    var pickUpLongitude: Double!
    var locationManager:CLLocationManager!
    var orderNumberforOrder:String!
    let ref = Database.database().reference()

//     init(deliverLatitude: Double, deliverLongitude: Double, orderStatus:String,pickUpLatitude:Double,pickUpLongitude:Double )
    
    init(orderDict:Dictionary<String,Any>)
    {
       
        if let deliverylat = orderDict[OrderDictionaryenum.deliveryLat.instance] as? String
        {
            if deliverylat  != ""
            {
            self.deliverLatitude = Double(deliverylat)
            }
            else
            {
                self.deliverLatitude = 0.000000
//                print("deliverLatitude:",deliverLatitude)
            }
        }
        else
        {
            self.deliverLatitude = 0.000000
//            print("deliverLatitude:",deliverLatitude)
        }
        
        
        if let deliverylong = orderDict[OrderDictionaryenum.deliveryLong.instance] as? String
        {
            if deliverylong   != ""
            {
                self.deliverLongitude = Double(deliverylong)
            }
            else
            {
                self.deliverLongitude = 0.000000
            }
        }
        else
        {
            self.deliverLongitude = 0.000000
            
        }
        
        self.orderNumberforOrder = orderDict[OrderDictionaryenum.ordernumber.instance] as? String

        
        self.orderStatus = "4"
        
        if  let pickuplong  = orderDict[OrderDictionaryenum.pickupLong.instance] as? String
        {
            if pickuplong   != ""
            {
                self.pickUpLongitude = Double(pickuplong)
            }
            else
            {
                self.pickUpLongitude = 0.000000
            }
        }
        else
        {
            self.pickUpLongitude = 0.000000
            
        }
        
        if let pickuplat = orderDict[OrderDictionaryenum.pickupLat.instance] as? String
        {
            if pickuplat   != ""
            {
                self.pickUpLatitude = Double(pickuplat)
            }
            else
            {
                self.pickUpLatitude = 0.000000
            }
        }
        else
        {
            self.pickUpLatitude = 0.000000
            
        }
        
    
    }
    
    
    func startUpdateLocation() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
        }
        else{
        }
        switch(CLLocationManager.authorizationStatus()) {
        case .notDetermined, .restricted, .denied:
            print("No access")
            
        case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
           
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
                locationManager!.allowsBackgroundLocationUpdates = true
                locationManager!.pausesLocationUpdatesAutomatically = false
               // locationManager.startMonitoringSignificantLocationChanges()
           
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        let latitude =  Double(String(format: "%.6f", userLocation.coordinate.latitude))!
        let longitude = Double(String(format: "%.6f", userLocation.coordinate.longitude))!
        print("latitude = ",latitude)
        print("longitude = ",longitude)
//        print("user latitude = \(userLocation.coordinate.latitude)")
//        print("user longitude = \(userLocation.coordinate.longitude)")
       // locationManager.stopUpdatingLocation()
       // makeServerRequestForOrder(latitude: "\(userLocation.coordinate.latitude)", longitude: "\(userLocation.coordinate.longitude)")
//        sendValuesToFirebase(mLattitude: userLocation.coordinate.latitude, mLongitude: userLocation.coordinate.longitude)
        sendValuesToFirebase(mLattitude: latitude, mLongitude: longitude)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted")
            
        case .denied:
            print("User denied access to location")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways:
           
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
                locationManager!.allowsBackgroundLocationUpdates = true
                locationManager!.pausesLocationUpdatesAutomatically = false

               // locationManager.startMonitoringSignificantLocationChanges()
            
        case .authorizedWhenInUse:
            print("Location status is OK")
            
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
                locationManager!.allowsBackgroundLocationUpdates = true
                locationManager!.pausesLocationUpdatesAutomatically = false

               // locationManager.startMonitoringSignificantLocationChanges()
            
        }
    }
    
    
    func sendValuesToFirebase(mLattitude:Double,mLongitude:Double)
    {
        
        let orderItem = OrderItem(deliverLatitude: self.deliverLatitude, deliverLongitude: self.deliverLongitude, mLattitude: mLattitude, mLongitude: mLongitude, orderStatus: self.orderStatus, pickUpLatitude: self.pickUpLatitude, pickUpLongitude: self.pickUpLongitude)
        
        let orderItemRef = self.ref.child("locations").child(UserDefaultUtils.getUserId()).child(self.orderNumberforOrder).child("123")
        
        orderItemRef.setValue(orderItem.toAnyObject())
        print("orderItem:",orderItem)
        
    }
    
    

}
