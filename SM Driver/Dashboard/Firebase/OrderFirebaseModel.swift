//
//  OrderFirebaseModel.swift
//  SM Driver
//
//  Created by mac-2 on 07/08/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import Firebase




   struct OrderItem{
    
    let ref: DatabaseReference?
    let deliverLatitude: Double
    let deliverLongitude: Double
    let mLattitude: Double
    var mLongitude: Double
    var orderStatus:String
    let pickUpLatitude: Double
    var pickUpLongitude: Double
    
    init(deliverLatitude: Double, deliverLongitude: Double, mLattitude: Double, mLongitude: Double,orderStatus:String,pickUpLatitude:Double,pickUpLongitude:Double ) {
        self.ref = nil
        self.deliverLatitude = deliverLatitude
        self.deliverLongitude = deliverLongitude
        self.mLattitude = mLattitude
        self.mLongitude = mLongitude
        self.orderStatus = orderStatus
        self.pickUpLatitude = pickUpLatitude
        self.pickUpLongitude = pickUpLongitude

    }
    
//    init?(snapshot: DataSnapshot) {
//        guard
//            let value = snapshot.value as? [String: AnyObject],
//            let name = value["name"] as? String,
//            let addedByUser = value["addedByUser"] as? String,
//            let completed = value["completed"] as? Bool else {
//                return nil
//        }
//
//        self.ref = snapshot.ref
//        self.key = snapshot.key
//        self.name = name
//        self.addedByUser = addedByUser
//        self.completed = completed
//    }
    
    func toAnyObject() -> Any {
        return [
            "deliverLatitude": deliverLatitude,
            "deliverLongitude": deliverLongitude,
            "mLattitude": mLattitude,
            "mLongitude": mLongitude,
            "orderStatus": orderStatus,
            "pickUpLatitude": pickUpLatitude,
            "pickUpLongitude": pickUpLongitude,
            
        ]
    }
}
