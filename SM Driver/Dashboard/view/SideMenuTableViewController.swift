//
//  SideMenuTableViewController.swift
//  SM Driver
//
//  Created by mac-2 on 04/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit
import SideMenu


class SideMenuTableViewController: UITableViewController {

    var presenter: ViewToPresnter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        tableView.tableFooterView = UIView(frame: .zero)
         tableView.register(UINib(nibName: "HeaderViewCell", bundle: nil), forCellReuseIdentifier: "HeaderViewCell")
      if #available(iOS 13.0, *) {


           let statusBar1 =  UIView()
           statusBar1.frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame as! CGRect
           statusBar1.backgroundColor = UIColor.green

           UIApplication.shared.keyWindow?.addSubview(statusBar1)

        } else {

           let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
           statusBar1.backgroundColor = UIColor.green
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
       navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let completeOrderController = CompleteOrderRouter.createModule()
            self.navigationController?.pushViewController(completeOrderController, animated: true)
        }
        else if indexPath.row == 2 {
            showLogoutAlert()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderViewCell") as! HeaderViewCell
        
        headerCell.profile_pic.layer.cornerRadius = 25;
        headerCell.profile_pic.layer.masksToBounds = true
        headerCell.user_name.text = UserDefaultUtils.getUserName()
        headerCell.user_email.text = UserDefaultUtils.getUserEmail()
        
         return headerCell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }


    func showLogoutAlert() {
        let message : String = "Do you want logout ?"
        let controller = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
        let action2 = UIAlertAction.init(title: "Yes", style: .default, handler: { (UIAlertAction) in
            // Do something with code
            print("Yes logout")
            self.doLogoutWork()
            })
        controller.addAction(action1)
        controller.addAction(action2)
        self.present(controller, animated: true, completion: nil)
    }
    func doLogoutWork() {
        dismiss(animated: true, completion: nil)


        UserDefaultUtils.setLoggedIn(isLogin: false)
        UserDefaultUtils.setUserId(userId: "0")
        let dashboard = LoginRouter.createModule()
        let navigationController = UINavigationController(rootViewController: dashboard)
        navigationController.isNavigationBarHidden = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
}
