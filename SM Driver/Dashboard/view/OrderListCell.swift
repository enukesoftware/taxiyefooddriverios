//
//  OrderListCell.swift
//  SM Driver
//
//  Created by mac-2 on 26/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class OrderListCell: UITableViewCell {

    @IBOutlet weak var time_left: UILabel!
    @IBOutlet weak var deliver_address: UILabel!
    @IBOutlet weak var deliverd_name: UILabel!
    @IBOutlet weak var pickup_address: UILabel!
    @IBOutlet weak var pickup_name: UILabel!
    
    var accpectOrder : CellToViewProtocol?
    
    var order:OrderList?
        var timer : Timer!
    
    var timeLeft : Int?
    
    var indexOfCell:Int?
    
    var id : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func accpect_order_action(_ sender: Any) {
        if let orderData = order {
            accpectOrder?.accpectOrderAction(order: orderData)
            timer.invalidate()


        }
    }
    
   @objc func updateTimer(){
   // timeLeft = 30
   // print("timeleft",timeLeft!)
    
    if timeLeft == -1
    {
        timer.invalidate()
        if let indexPath = indexOfCell {
            accpectOrder?.timerComplete(index: indexPath, id: self.id!)
            
        }

        return
    }
    
        if let seconds = timeLeft  {
            if seconds >= 0{
                time_left.text = "\(seconds)"
                timeLeft = seconds-1
            }
            else{
                
                time_left.text = "Done"
                timer.invalidate()
                if let indexPath = indexOfCell {
                    accpectOrder?.timerComplete(index: indexPath, id: self.id!)
              
                }
            }
        }
    }
}
