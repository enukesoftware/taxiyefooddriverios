//
//  CustomerInfomationTableViewCell.swift
//  SM Driver
//
//  Created by Mac on 26/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import UIKit

class CustomerInfomationTableViewCell: UITableViewCell {

    fileprivate let application =  UIApplication.shared
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var callCustomerButton: UIButton!
    @IBOutlet weak var customerName: UILabel!
    
    @IBOutlet weak var phoneImg: UIButton!
    @IBOutlet weak var customerImg: UIImageView!
    @IBOutlet weak var customerMobileNum: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        phoneImg.tintColor = UIColor.white
        phoneImg.imageEdgeInsets = UIEdgeInsets(top:8, left: 10, bottom: 5, right: 10)
        customerImg.layer.cornerRadius = 25
        customerImg.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        containerView.setCornerradiusView(amount: 10)

        // Configure the view for the selected state
    }
    
    @IBAction func customerCallBtn(_ sender: Any) {
        print("customerMobileNum:",customerMobileNum)
        if let phoneUrl = URL(string: "tel://9809088798"){
            if application.canOpenURL(phoneUrl){
                application.open(phoneUrl, options: [:], completionHandler:nil)
            }
            else{
                print("not call")
            }
        }
    


    }
    
}
