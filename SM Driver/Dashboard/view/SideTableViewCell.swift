//
//  SideTableViewCell.swift
//  SM Driver
//
//  Created by Mac on 23/01/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import UIKit

class SideTableViewCell: UITableViewCell {
    
@IBOutlet weak var nameList: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
