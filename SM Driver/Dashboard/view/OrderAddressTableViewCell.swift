//
//  OrderAddressTableViewCell.swift
//  SM Driver
//
//  Created by mac-2 on 30/07/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class OrderAddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var locationNamelabel: UILabel!
    

    @IBOutlet weak var locationTypeLabel: UILabel!
    
    @IBOutlet weak var locationAddresslabel: UILabel!
    
    @IBOutlet weak var mapButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        containerView.setCornerradiusView(amount: 10)

        // Configure the view for the selected state
    }
    
    func setValueInLabel(type:String, locationName:String?)
    {
        if type == "pickup"
        {
            locationTypeLabel.text = "Pick up From"
        }
        else
        {
            locationTypeLabel.text = "Deliver To"
        }
        
        if let address = locationName
        {
            locationAddresslabel.text = address
        }
    }
    
}
