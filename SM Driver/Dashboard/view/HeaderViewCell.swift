//
//  HeaderViewCell.swift
//  SM Driver
//
//  Created by mac-2 on 24/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class HeaderViewCell: UITableViewCell {

    @IBOutlet weak var user_email: UILabel!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var profile_pic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
