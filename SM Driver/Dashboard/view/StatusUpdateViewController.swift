//
//  StatusUpdateViewController.swift
//  SM Driver
//
//  Created by mac-2 on 05/08/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class StatusUpdateViewController: UIViewController {
    
    
    @IBOutlet weak var VIewContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var pickedUp: UIButton!
    
    
    @IBOutlet weak var delivered: UIButton!
    
    var updateOrder : OrderUpdateStatus?

    
    @IBOutlet weak var statusLabel: UILabel!
    
    
    @IBOutlet weak var cancelButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         containerView.setCornerradiusView(amount: 10)
         VIewContainer.setCornerradiusView(amount: 10)
        pickedUp.setCornerradiusView(amount: 20)
        delivered.setCornerradiusView(amount: 20)
        cancelButton.setBorder(radius: 5, color: UIColor.red)
        
      

        // Do any additional setup after loading the view.
    }


    @IBAction func statusUpdateBtnClicked(_ sender: UIButton) {
       
        
        sender.isSelected = !sender.isSelected
        
          var status = ""
        var reason = ""
        if sender.tag == 0
        {
            statusLabel.isHidden = true
            reason = "order picked up"
            status = "4"
        }
        else{
            
            if UserDefaultUtils.getCurrentOrderStatus() != "4"
            {
                statusLabel.isHidden = false
                sender.isSelected = !sender.isSelected
                return
                
            }
            reason = "order delivered up"
            status = "5"


        }
        
        updateOrder?.orderUpdateAction(reason: reason, status: status)
        
        
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
