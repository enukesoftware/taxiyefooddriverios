//
//  MenuViewController.swift
//  MultiSideBar
//
//  Created by Mac on 20/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate{
    func slideMenuItemSelectedAtIndex(_ index : Int32)
    //func openViewControllerBasedOnIdentifier(_ strIdentifier:String)
}


class MenuViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    var presenter: ViewToPresnter?

   
    var btnMenu : UIButton!
    var delegate : SlideMenuDelegate?
    
    var sidemenuList: [String] = ["Profile","Accept Order", "Complete Order","Change Password", "Logout"]
    
     @IBOutlet weak var sideTableView: UITableView!
    @IBOutlet weak var sideMenuCloseOverLap: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        sideTableView.delegate = self
        sideTableView.dataSource = self
        sideTableView.register(UINib(nibName: "HeaderViewCell", bundle: nil), forCellReuseIdentifier: "HeaderViewCell")
        sideTableView.tableFooterView = UIView(frame: .zero)
//        if #available(iOS 13.0, *) {
//
//
//           let statusBar1 =  UIView()
//           statusBar1.frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame as! CGRect
//           statusBar1.backgroundColor = UIColor.green
//
//           UIApplication.shared.keyWindow?.addSubview(statusBar1)
//
//        } else {
//
//           let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//           statusBar1.backgroundColor = UIColor.green
//        }
    }
    override func viewDidAppear(_ animated: Bool) {
            self.sideTableView.reloadData()

    }
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sidemenuList.count 
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var  cell = sideTableView.dequeueReusableCell(withIdentifier:"sidemenu") as!  SideTableViewCell
    
        if (indexPath.row == 0){
            let  headerCell = sideTableView.dequeueReusableCell(withIdentifier: "HeaderViewCell") as! HeaderViewCell
            headerCell.user_name.text = UserDefaultUtils.getUserName()
            headerCell.user_email.text = UserDefaultUtils.getUserEmail()
           
            let imageString = UserDefaultUtils.getProfileImageURL()
            if ( imageString != "") {
                var escapedString = imageString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)

                var url:NSURL = NSURL(string: escapedString as! String)!
                var data = NSData(contentsOf: url as URL)
                if data != nil{
                    headerCell.profile_pic.image = UIImage(data: data! as Data)
                    headerCell.profile_pic.layer.masksToBounds = true
                    headerCell.profile_pic.layer.cornerRadius = 42
                }
             }
            return headerCell
        }
        else{
            
            cell.nameList.text = sidemenuList[indexPath.row]
        }
         return cell
    }
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row != 1{
        delegate?.slideMenuItemSelectedAtIndex(Int32(indexPath.row))
        self.sidemenuClose()
    }
 func sidemenuClose(){
    
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if (self.delegate != nil){
            var index = Int32(1)
         if (( self.sideMenuCloseOverLap) != nil){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
        self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor  = UIColor.clear
        }) { (finished) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    func showLogoutAlert() {
        let message : String = "Do you want logout ?"
        let controller = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
        let action2 = UIAlertAction.init(title: "Yes", style: .default, handler: { (UIAlertAction) in
            // Do something with code
            print("Yes logout")
            self.doLogoutWork()
            })
        controller.addAction(action1)
        controller.addAction(action2)
        self.present(controller, animated: true, completion: nil)
    }
    
    func doLogoutWork() {
        dismiss(animated: true, completion: nil)

        UserDefaultUtils.setLoggedIn(isLogin: false)
        UserDefaultUtils.setUserId(userId: "0")
        let dashboard = LoginRouter.createModule()
        let navigationController = UINavigationController(rootViewController: dashboard)
        navigationController.isNavigationBarHidden = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
 
    
    @IBAction func closeSideMenubtnPressed(_ sender: UIButton) {
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if (self.delegate != nil){
            var index = Int32(sender.tag)
            if (sender == self.sideMenuCloseOverLap){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
        self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor  = UIColor.clear
        }) { (finished) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
 
    
    
}
