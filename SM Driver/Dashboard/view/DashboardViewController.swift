//
//  DashboardViewController.swift
//  SM Driver
//
//  Created by mac-2 on 04/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit
import SideMenu
import MBProgressHUD
import CoreLocation
import Firebase
import UserNotifications


class DashboardViewController:BaseViewController , PresenterToView , CLLocationManagerDelegate ,  CellToViewProtocol,OrderUpdateStatus{
   
   
    @IBOutlet weak var markStatusButton: UIButton!
    
    @IBOutlet weak var orderListTableView: UITableView!
    @IBOutlet weak var orderDataLabel: UILabel!
    
     var progressHUD: MBProgressHUD!
    
    var presenter: ViewToPresnter?
    
    var orderList:[OrderList] = []
    
    var loctaionToFirebase:SendLocationFirebase?
    
    var current:OrderList?

    var currentTimeleftDict = Dictionary<String,Int>()
   // let menu = SideMenuManager.default

    

    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  setupMenuUI()
        addSlideMenuButton()
      // setSidedraweMenu()
//        self.navigationController?.navigationBar.barTintColor = UIColor.black

//        self.navigationItem.title = "Accept Order"
       

        // Do any additional setup after loading the view.
        
       // presenter?.checkLocationStatus()

        orderListTableView.isHidden = true
        orderDataLabel.isHidden = false
        
        markStatusButton.isHidden = true
        
        
        orderListTableView.delegate = self
        orderListTableView.dataSource = self
        
        orderListTableView.tableFooterView = UIView(frame: .zero)
        
        orderListTableView.register(UINib(nibName: "OrderIdTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderIdTableViewCell")
        orderListTableView.register(UINib(nibName: "OrderAddressTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderAddressTableViewCell")
        
        orderListTableView.register(UINib(nibName: "CustomerInfomationTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomerInfomationTableViewCell")
        orderListTableView.rowHeight = UITableView.automaticDimension
        orderListTableView.estimatedRowHeight = UITableView.automaticDimension
        
    }
    
    // MARK: viewwillappear
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationItem.title = "Accept Order"

        setViewforAcceptedOrder()
        
        }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        presenter?.stopTimer()
    }
    
    
    
    
    func sendValuesToFirebase()
    {
//        loctaionToFirebase = SendLocationFirebase(deliverLatitude: 28.4848783, deliverLongitude: 77.0589723, orderStatus: "3", pickUpLatitude: 28.4658934, pickUpLongitude: 77.0356671)
        
     if let orderDictionary = UserDefaultUtils.getOrderDictionary()
     {
        loctaionToFirebase = SendLocationFirebase(orderDict: orderDictionary)
        //Constants.firebasesendData = SendLocationFirebase(orderDict: orderDictionary)
         loctaionToFirebase?.startUpdateLocation()
        }
        

    }
    
    
    func setViewforAcceptedOrder()
    {
        
        if UserDefaultUtils.getOrderDictionary() != nil
        {
            orderListTableView.isHidden = false
            orderDataLabel.isHidden = true
            orderListTableView.reloadData()
            markStatusButton.isHidden = false

        }else
        {
            markStatusButton.isHidden = true
            markStatusButton.isHidden = true
            orderListTableView.isHidden = true
           

            presenter?.checkLocationStatus()
  
        }
        
        setRightBarButton()

        
    }
    
  

    private func setupMenuUI(){
//       menu.menuPresentMode = .menuSlideIn
//        menu.menuAnimationBackgroundColor = UIColor.white
    }
    
    private func setSidedraweMenu(){
        
//       menu.menuLeftNavigationController = self.storyboard!.instantiateViewController(withIdentifier: "UISideMenuNavigationController") as? UISideMenuNavigationController
        // Hide the back button
        self.navigationItem.hidesBackButton = true
        // Left navigation items
        addSlideMenuButton()

//        let leftArrow2BarButtonItem = UIBarButtonItem(image: UIImage(named: "drawer_icon"), style: .plain, target: self, action: #selector(self.onLeftArrow2Pressed(_:)))
//        self.navigationItem.leftBarButtonItems = [ leftArrow2BarButtonItem ]
      //  self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    private func setRightBarButton(){
      
        // Right navigation items
        if UserDefaultUtils.getDriverActiveStatus() {
        let rightBarButtonActive = UIBarButtonItem(image: UIImage(named: "deactive")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.onRightButtonAction))
        rightBarButtonActive.tag = 1
            self.navigationItem.rightBarButtonItems = [ rightBarButtonActive ]
            self.navigationController?.setNavigationBarHidden(false, animated: true)
             self.orderDataLabel.text = "No Order"
        }
        else {
            let rightBarButtonActive = UIBarButtonItem(image: UIImage(named: "active")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.onRightButtonAction))
            rightBarButtonActive.tag = 0
            self.navigationItem.rightBarButtonItems = [ rightBarButtonActive ]
            self.navigationController?.setNavigationBarHidden(false, animated: true)
             self.orderDataLabel.text = Constants.DEACTIVE_STATUS
        }
    }
    
    @objc func onLeftArrow2Pressed(_ sender: UIView)  {
        
//        let menu = self.storyboard!.instantiateViewController(withIdentifier: "UISideMenuNavigationController") as! UISideMenuNavigationController
//        let menu = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
       // addSlideMenuButton()

        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        // let menu = storyboard!.instantiateViewController(withIdentifier: "RightMenu") as! UISideMenuNavigationController
        
       //present(menu, animated: true, completion: nil)
       // present(self.menu.menuLeftNavigationController!, animated: true, completion: nil)

    }
    
    @objc func onRightButtonAction()  {
        // Right Button Action
        
        if UserDefaultUtils.getOrderDictionary() != nil
        {
            validationAlert(reason: "You cannot pick new order")
            return
        }
        self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)
        presenter?.statusBtnClick()
    }
    
    func statusUpdated() {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            self.presenter?.checkLocationStatus()
            let rightBtns = self.navigationItem.rightBarButtonItems
            let activate = rightBtns?[0]
            if activate?.tag == 1{
                activate?.image = UIImage(named: "active")?.withRenderingMode(.alwaysOriginal)
                activate?.tag = 0
                self.orderDataLabel.text = Constants.DEACTIVE_STATUS
                self.orderListTableView.isHidden = true
                self.orderDataLabel.isHidden = false
            }
            else{
                activate?.image = UIImage(named: "deactive")?.withRenderingMode(.alwaysOriginal)
                activate?.tag = 1
                self.orderDataLabel.text = "No Order"
            }
        }
    }
    
    func statusUpdateFail(error: String) {
        self.progressHUD.hide(animated: true)
        UniversalTaskExecuter.showOKAlertWith(message: error, title: "Error", self)
    }
    
    func updateOrderTable(orderList:[OrderList]) {
        self.orderList = orderList
        orderListTableView.reloadData()
        currentTimeleftDict = Dictionary<String,Int>()
        if orderList.count > 0 {
        orderListTableView.isHidden = false
        orderDataLabel.isHidden = true
        }
    }
    
    func accpectOrderAction(order: OrderList) {
        self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)
        presenter?.accpectOrderClick(order: order)
        
//        var Orderdict  = Dictionary<String,Any>()
//        Orderdict[""] = order.order_number
        
    }
    
    func accpectOrderSucess(order:OrderList) {
         self.progressHUD.hide(animated: true)
              print("order:",order)
               var Orderdict  = Dictionary<String,Any>()
                Orderdict[OrderDictionaryenum.ordernumber.instance] = order.order_number
                Orderdict[OrderDictionaryenum.orderAmount.instance] = order.amount
                Orderdict[OrderDictionaryenum.orderPickupAddress.instance] = getPickupAddress(order: order)
                Orderdict[OrderDictionaryenum.orderDeliveryAddress.instance] = getDeliverAddress(order: order)
         
               Orderdict[OrderDictionaryenum.orderUserName.instance] = order.first_name
               Orderdict[OrderDictionaryenum.orderUserContact.instance] = order.contact_number
        
               Orderdict[OrderDictionaryenum.CustomerImg.instance] = order.profile_image
//             print("url:",Orderdict[OrderDictionaryenum.CustomerImg.instance] )
            
                if let resturantName = order.restaurant_name
                {
                    Orderdict[OrderDictionaryenum.resturantName.instance] = resturantName
                    
                }
        
                if let resturantLat = order.restaurant_latitude
                {
                    Orderdict[OrderDictionaryenum.pickupLat.instance] = resturantLat
                }
        
                if let resturantLong = order.restaurant_longitude
                {
                    Orderdict[OrderDictionaryenum.pickupLong.instance] = resturantLong
                }
        
        let deliveryName =   (order.first_name == "" ? "": order.first_name ?? " ")+(order.last_name == "" ? "":order.last_name ?? " ")
        
                    Orderdict[OrderDictionaryenum.deliveryName.instance] = deliveryName
        
            let deliverylat = order.ship_json?.latitude
        
             if (deliverylat != nil)
                {
                    Orderdict[OrderDictionaryenum.deliveryLat.instance] = deliverylat
                    
                }
                else
                {
                    Orderdict[OrderDictionaryenum.deliveryLat.instance] = "0.000000"
                    
                }
           let deliverylong = order.ship_json?.longitude
           
           if (deliverylong != nil)
                {
                    Orderdict[OrderDictionaryenum.deliveryLong.instance] = deliverylong
                }
          else {
                 Orderdict[OrderDictionaryenum.deliveryLong.instance] = "0.000000"
                }
            
           UserDefaultUtils.setOrderDictionary(orderdict: Orderdict)
           UserDefaultUtils.setDriverActiveStatus(status: false)
           setViewforAcceptedOrder()

         UserDefaultUtils.setorderId(orderId: order.id!)
       
    }
    
    
    func getPickupAddress(order:OrderList) -> String
    {
        var pickupAddress:String = ""
        pickupAddress = order.restaurant_floor == "" ? "": order.restaurant_floor ?? " " + ", "
        
        if let restaurantStreet = order.restaurant_street{
            if restaurantStreet != ""{
                pickupAddress = pickupAddress + restaurantStreet + ", "
            }
        }
        
        if let restaurantArea = order.restaurant_area{
            if restaurantArea != ""{
                pickupAddress = pickupAddress + restaurantArea + ", "
            }
        }
        
        if let restaurantCity = order.restaurant_city{
            if restaurantCity != ""{
                pickupAddress = pickupAddress + restaurantCity
            }
        }
        
         return pickupAddress
    }
    
    
    
    func getDeliverAddress(order:OrderList) -> String
    {
        var deliverAddress:String = ""
        if let floor = order.ship_json?.address_json?.floor_unit{
            if floor != ""{
                deliverAddress = deliverAddress+floor+", "
            }
        }
        
        if let houseNo = order.ship_json?.address_json?.house_no {
            if houseNo != ""{
                deliverAddress = deliverAddress + houseNo + ", "
            }
        }
        
        if let street = order.ship_json?.address_json?.street {
            if street != "" {
                deliverAddress = deliverAddress + street + ", "
            }
        }
        
        if let areaname = order.ship_json?.address_json?.area_name {
            if areaname != "" {
                deliverAddress = deliverAddress + areaname + ", "
            }
        }
        
        if let city = order.ship_json?.address_json?.city{
            if city != "" {
                deliverAddress = deliverAddress + city
            }
        }
        return deliverAddress
    }
    
    
    func timerComplete(index: Int,id: String) {
        
        //orderList.indices.contains(index) &&
       if  UserDefaultUtils.getDriverActiveStatus()
       {
       if let indexOforder = orderList.firstIndex(where: {$0.id == id})
       {// 0

        orderList.remove(at: indexOforder)
        let indexPath = IndexPath(item: indexOforder, section: 0)
        orderListTableView.deleteRows(at: [indexPath], with: .fade)
        }
        }
    }
    
    
    
    @IBAction func markStatusButtonTapped(_ sender: UIButton)
    {
        let statusController = StatusUpdateViewController(nibName: "StatusUpdateViewController", bundle: nil)
        statusController.updateOrder = self
//        self.preferredContentSize = CGSize(width: 100, height: 400)
        statusController.modalPresentationStyle = .overCurrentContext
        present(statusController, animated: true, completion: nil)
    }
    
   
    // get staus foer pickup or deliver
    func orderUpdateAction(reason:String,status:String)
    {
        dismiss(animated: true, completion: nil)
        
        self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)

        self.presenter?.updateOrder(reason: reason, status: status)

    }

    
    // get staus from presenter for update order request 
    func statusUpdateOrder(response:String)
    {
        self.progressHUD.hide(animated: true)
        
        if response == "start tracking location"
        {
            sendValuesToFirebase()
        }
        else
        {
            if response == "delivered"
            {
                loctaionToFirebase?.locationManager.stopUpdatingLocation()
            }
        UserDefaultUtils.setDriverActiveStatus(status: true)
        UserDefaultUtils.setnilOrderDictionary()
        self.setViewforAcceptedOrder()
        }
        
    }
    
    
    // alert and pdelegate to ask reason of cancelation

    @objc func cancelOrder()
    {
        if UserDefaultUtils.getCurrentOrderStatus() != nil && UserDefaultUtils.getCurrentOrderStatus() == "4"
        {
            validationAlert(reason:"You cannot cancel the order as you have already pickup the order")
        }
        
        else
        {
        let alertController = UIAlertController(title: "Please Enter the reason for cancelling the order ", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Reason"
        }
        
        let saveAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { alert -> Void in
         
            let firstTextField = alertController.textFields![0] as UITextField
            
            if firstTextField.text == ""
            {
                self.validationAlert(reason: "Please fill the Reason")
            }
            else
            {
            self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)
            self.presenter?.updateOrder(reason: firstTextField.text,status: "6")
            }
        })
        
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    
    func validationAlert(reason:String)
    {
         let alertController = UIAlertController(title: reason, message: "", preferredStyle: UIAlertController.Style.alert)
       
        let saveAction = UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { alert -> Void in
            if reason == "Please fill the Reason"
            {
            self.cancelOrder()
            }
        
        })
        
        alertController.addAction(saveAction)

        self.present(alertController, animated: true, completion: nil)

        
    }
//    Customer Call
   

    @objc func callCustomer(sender:UIButton){
        if sender.accessibilityHint! != nil{
            let contact =  "tel://" + sender.accessibilityHint!
                 
                   let url:NSURL = NSURL(string: contact)!

                   if (UIApplication.shared.canOpenURL(url as URL))
                   {
                       UIApplication.shared.openURL(url as URL)
                   }
              }
        else{
            
        }
       
    }
    
    
    
// open google maps to show current distance
    @objc  func openGooglemaps(sender:UIButton)
     {
        //restaurant_longitude: Optional("77.026634"), restaurant_latitude: Optional("28.459497"),
        
        var orderDictValue = UserDefaultUtils.getOrderDictionary()
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {

            if sender.tag == 1
            {
            if let pickupLat = orderDictValue![OrderDictionaryenum.pickupLat.instance] as? String, let pickupLong = orderDictValue![OrderDictionaryenum.pickupLong.instance] as? String
            {
                UIApplication.shared.open(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(Float(pickupLat)!),\(Float(pickupLong)!)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
            }
            }
            else if sender.tag == 2{
                if let deliverLat = orderDictValue![OrderDictionaryenum.deliveryLat.instance] as? String, let deliverLong = orderDictValue![OrderDictionaryenum.deliveryLong.instance] as? String
                {
                    UIApplication.shared.open(NSURL(string:
                        "comgooglemaps://?saddr=&daddr=\(Float(deliverLat)!),\(Float(deliverLong)!)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
                }
                
            }
            
//            UIApplication.shared.open(NSURL(string:
//                "comgooglemaps://?saddr=&daddr=\(Float("28.459497")!),\(Float("77.026634")!)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)

            
        }
    else
        {
            print("Can't use com.google.maps://");
        }
        
    }
    
    
    
    
    
    
}
