//
//  OrderIdTableViewCell.swift
//  SM Driver
//
//  Created by mac-2 on 30/07/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class OrderIdTableViewCell: UITableViewCell {

    @IBOutlet weak var orderidValueLabel: UILabel!
    
    @IBOutlet weak var amountValueLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var heightCancelButton: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cancelButton.setCornerradius(amount: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValueToLabel(orderid:String?,amount:String?)
    {
        statusLabel.text = "READY"
        
        if let orderidvalue = orderid
        {
            orderidValueLabel.text = orderidvalue
        }
        
        if let orderamountvalue = amount
        {
            amountValueLabel.text = "$" + orderamountvalue
        }
        
    }
    
}
