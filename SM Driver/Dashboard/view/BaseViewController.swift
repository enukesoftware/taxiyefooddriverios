//
//  BaseViewController.swift
//  MultiSideBar
//
//  Created by Mac on 20/01/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        switch(index){
        case 0:            self.openViewControllerBasedOnIdentifier("ProfileViewController")
            break
        case 1:
            let dashboard = DashoardRouter.createModule()
            self.navigationController?.pushViewController(dashboard, animated: true)
            break
        case 2:
            let completeOrderController = CompleteOrderRouter.createModule()
            self.navigationController?.pushViewController(completeOrderController, animated: true)
            break
        case 3:
            let passwordChangeController = PasswordChangeRouter.createModule()
            self.navigationController?.pushViewController(passwordChangeController, animated: true)
            break
        case 4:
            showLogoutAlert()
            break
        default:
            print("defaults\n", terminator: "")
        }
    }
    
    func showLogoutAlert() {
        let message : String = "Do you want logout ?"
        let controller = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
        let action2 = UIAlertAction.init(title: "Yes", style: .default, handler: { (UIAlertAction) in
            // Do something with code
            print("Yes logout")
            self.doLogoutWork()
        })
        controller.addAction(action1)
        controller.addAction(action2)
        self.present(controller, animated: true, completion: nil)
    }
    func doLogoutWork() {
        dismiss(animated: true, completion: nil)
        UserDefaultUtils.setLoggedIn(isLogin: false)
        UserDefaultUtils.setUserId(userId: "0")
        let dashboard = LoginRouter.createModule()
        let navigationController = UINavigationController(rootViewController: dashboard)
        navigationController.isNavigationBarHidden = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let profileViewController = self.storyboard?.instantiateViewController(withIdentifier: strIdentifier) as! ProfileViewController
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
}
