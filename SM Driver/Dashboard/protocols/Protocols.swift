//
//  Protocols.swift
//  SM Driver
//
//  Created by mac-2 on 04/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation


protocol ViewToPresnter : class {
    var view: PresenterToView?{ get set }
    func statusBtnClick()
    func moveToLogin()
    func checkLocationStatus()
    func accpectOrderClick(order:OrderList)
    func updateOrder(reason:String?,status:String)
    func stopTimer()
}

protocol PresenterToView : class {
    func statusUpdated()
    func statusUpdateFail(error : String)
    func updateOrderTable(orderList:[OrderList])
    func accpectOrderSucess(order:OrderList)
    func statusUpdateOrder(response:String)
}

protocol InteractorToPresnter : class {
    var interactor : PresenterToInteractor?{ get set }
    func stausUpdateSucess()
    func stausUpdateFail(error:String)
    func sendOrderListData(orderList:[OrderList])
    func accpectOrderStatus(order:OrderList)
    func updateStausOfOrder(status:String?)
    
}

protocol PresenterToInteractor : class{
    func statusActive()
    func statusDeactive()
    func startUpdateLocation()
    func accpectOrder(order:OrderList)
    func callapiUpdateOrder(reason:String?,status:String)
    
}

protocol PresenterToRouter {
    func moveToNext()
}

protocol CellToViewProtocol {
    func accpectOrderAction(order: OrderList)
func timerComplete(index: Int,id: String)
    
}


protocol OrderUpdateStatus {
    func orderUpdateAction(reason:String,status:String)
    
}
