//
//  RegiterInterector.swift
//  SM Driver
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//


import Foundation
import SwiftyJSON


class RegisterInterector: RegisterPresenterToInteractorProtocol   {
    
    var presenter: RegisterInteractorToPresenter?

    func doRegisterWith(body:[String:Any]) {
//        print(body)
        
        if(!Connectivity.isConnectedToInternet){
            presenter?.registerFail(error: "Internet is not connected")
            return
        }
        
        let inputDic:[String:Any] = body
               
               ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.REGISTER_METHOD,headerString: "",  onCompletion: {
                   (json: JSON, err: NSError?) in
                   if err != nil{
                       self.presenter?.registerFail(error: err.debugDescription)
                   }
                   else{
                     //  print(json.stringValue)
                       if json["success"].boolValue == true {
                        print("json:",json["success"])
                           self.presenter?.registerSucess()
                       }
                       else{
                           let message = json["message"].stringValue
                           self.presenter?.registerFail(error: message)
                       }
                   }
               })
    }
}
