//
//  RegisterRouter.swift
//  SM Driver
//
//  Created by Mac on 14/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation
import UIKit

class RegisterRouter : RegisterPresenterToRouterProtocol {
    
    static func createModule() -> RegisterViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController

        let presenter: RegisterPresenter = RegisterPresenter()
        let interactor: RegisterInterector = RegisterInterector()
        let router:RegisterRouter = RegisterRouter()

        view.presenterDelegate = presenter
        presenter.view = view  
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter

        return view
        
    }
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }

}

