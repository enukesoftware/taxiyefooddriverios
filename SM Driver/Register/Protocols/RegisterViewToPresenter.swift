//
//  RegisterViewToPresenter.swift
//  SM Driver
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterViewToPresenter: class {
    func makeRegister(body:[String:Any])
}
