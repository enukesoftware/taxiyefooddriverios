//
//  RegisterPresenterToInteractorProtocol.swift
//  SM Driver
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation

protocol RegisterPresenterToInteractorProtocol: class {
    func doRegisterWith(body:[String:Any])
}
