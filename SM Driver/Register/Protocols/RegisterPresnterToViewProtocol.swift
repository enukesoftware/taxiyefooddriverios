//
//  RegisterPresnterToViewProtocol.swift
//  SM Driver
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation

protocol RegisterPresnterToViewProtocol : class {
    
    func RegisterSucess()
    func RegisterError(error:String)
}
