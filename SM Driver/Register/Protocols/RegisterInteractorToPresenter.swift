//
//  RegisterInteractorToPresenter.swift
//  SM Driver
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation

protocol RegisterInteractorToPresenter:class {
    func registerSucess()
    func registerFail(error:String)
}
