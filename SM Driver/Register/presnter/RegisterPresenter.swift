//
//  RegisterPresenter.swift
//  SM Driver
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//


import Foundation
import UIKit

class RegisterPresenter : RegisterInteractorToPresenter , RegisterViewToPresenter {
    
    var view: RegisterPresnterToViewProtocol?
    var interactor: RegisterPresenterToInteractorProtocol?
    var router : RegisterPresenterToRouterProtocol?
    
    
    func registerSucess() {
        view?.RegisterSucess()
    }
    func registerFail(error: String) {
        view?.RegisterError(error: error)
    }
    
    func makeRegister(body:[String:Any]) {
        interactor?.doRegisterWith (body: body)
    }
    
}
