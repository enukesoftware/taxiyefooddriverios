//
//  CountryCodeViewController.swift
//  SM Driver
//
//  Created by Mac on 06/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import UIKit
protocol CountryCodeViewControllerDelegate {
    func countrycode(data: [String : Any])
}

class CountryCodeViewController: UIViewController {
    var countrycodeindex: Int?
    var countrycode: String?
    
    var delegate: CountryCodeViewControllerDelegate?

    
//    var jsonDict: Any?
    var countriesArray = [[String:Any]]()
//    var isSearching = false
   
  

    @IBOutlet weak var countryCodeTableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var filteredData = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self

        let countryname :String = "CountryCode"
        readJSONFromFile(fileName: countryname)
    }
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    
 func readJSONFromFile(fileName: String)
       {
           if let path = Bundle.main.path(forResource: fileName, ofType: "json"){
               do {
                   let fileUrl = URL(fileURLWithPath: path)
                var data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                var jsonDict = try? JSONSerialization.jsonObject(with: data)
                var countries =   jsonDict as! [String:Any]
                countriesArray = countries["countries"] as! [[String : Any]]
                filteredData = countriesArray
               } catch {
                   // Handle error here
               }
           }
       }

    
    @IBAction func countrycodeDoneBtnPressed(_ sender: Any) {
        if countrycode != nil && countrycodeindex != nil{
            var data = [String : Any]()
            data["tag"] = index
            data["value"] = countrycode
            self.delegate?.countrycode(data:data)
            self.navigationController?.popViewController(animated: true)
           // self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
//        else{
//             self.navigationController?.popViewController(animated: true)
//           //  self.presentingViewController?.dismiss(animated: true, completion: nil)
//        }
        
    }
    
}


extension CountryCodeViewController :UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate{
    
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            filteredData = searchText.isEmpty ? countriesArray : countriesArray.filter { (object) -> Bool in
                    guard let countryName = object["countryName"] as? String else {return false}
                    guard let countryCode = object["countryCode"] as? String else {return false}
                    if  countryName.contains(searchText){
                            return countryName.contains(searchText) ?? false
                        }
                    else {
                            return countryCode.contains(searchText) ?? false
                        }
            }
            countryCodeTableView.reloadData()

        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           // print(countriesArray.count)
           return filteredData.count
    //        return countriesArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = countryCodeTableView.dequeueReusableCell(withIdentifier: "countrycode", for: indexPath) as! CountryCodeTableViewCell
            
    //        var dict = countriesArray[indexPath.row]

            var dict = filteredData[indexPath.row]

            cell.textLabel?.text = dict["countryName"] as! String
            cell.detailTextLabel!.text = dict["countryCode"] as! String
            cell.textLabel?.textColor = UIColor.white
            cell.detailTextLabel?.textColor = UIColor.white// <- Changed color here

            return cell
        }

        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            var dict = filteredData[indexPath.row]

            countrycode = dict["countryCode"] as! String
            if countrycode != nil && countrycodeindex != nil{
                var data = [String : Any]()
                data["tag"] = index
                data["value"] = countrycode
                self.delegate?.countrycode(data:data)
                self.navigationController?.popViewController(animated: true)
               // self.presentingViewController?.dismiss(animated: true, completion: nil)
                }
    //        else{
    //             self.navigationController?.popViewController(animated: true)
    //           //  self.presentingViewController?.dismiss(animated: true, completion: nil)
    //        }
        }
}
