//
//  RegisterViewController.swift
//  SM Driver
//
//  Created by Mac on 06/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD


class RegisterViewController: UIViewController,CountryCodeViewControllerDelegate,UITextFieldDelegate {
    
    
    var progressHUD: MBProgressHUD!
    var presenterDelegate : RegisterViewToPresenter?

    @IBOutlet weak var fnameImg: UIImageView!
    @IBOutlet weak var cpasswordImg: UIImageView!
    @IBOutlet weak var passwordImg: UIImageView!
    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var emailImg: UIImageView!
    @IBOutlet weak var lnameImg: UIImageView!
    
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var countryCode: UIButton!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var conformPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    @IBOutlet weak var phoneImg: UIImageView!
    var countrycodevalue: String? = "251"
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
   
    @IBOutlet weak var registerbtnview: UIView!
    override func viewDidLoad() {
//        self.navigationController?.isNavigationBarHidden = true

        super.viewDidLoad()
         fnameImg.tintColor = UIColor.white
         lnameImg.tintColor = UIColor.white
         emailImg.tintColor = UIColor.white
         countryImg.tintColor = UIColor.white
         passwordImg.tintColor = UIColor.white
         cpasswordImg.tintColor = UIColor.white
         phoneImg.tintColor = UIColor.white
         self.textPlaceholder()
        registerbtnview.layer.cornerRadius = 25
        password.isSecureTextEntry = true
        conformPassword.isSecureTextEntry = true
        countryCode.contentHorizontalAlignment = .left
        self.phoneNumber.delegate = self
        self.countryCode.setTitle("251", for: .normal)

    }
    
    func textPlaceholder(){
        firstName.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        lastName.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        phoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        password.attributedPlaceholder = NSAttributedString(string: "Passwrod", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        conformPassword.attributedPlaceholder = NSAttributedString(string: "Conform password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = phoneNumber.text
        let newLength = text!.characters.count + string.characters.count
        return newLength <= 10
    }
    @IBAction func registerBtnPressed(_ sender: Any) {
    self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)

        let firstname = firstName.text!
        let lastname = lastName.text!
        let Email = email.text!
        let phone = phoneNumber.text!
        let passWord = password.text!
        let conpassword = conformPassword.text!

        let msg = validtationChecked(name:firstname,lname:lastname, mobileNum:phone,email:Email,pass:passWord,cpassword:conpassword,countrycodevalue:countrycodevalue!)
        
        if (msg){
            let body:[String:Any] = [
                "email":Email,
                "password":passWord,
                "contact_number":phone,
                "first_name":firstname,
                "last_name":lastname,
                "countrycode":countrycodevalue!,
                        ]
            presenterDelegate?.makeRegister(body:body)
        }
//        if (msg){
//            let body:[String:Any] = [
//            "email":Email,
//            "password":passWord,
//            "contact_number":phone,
//            "first_name":firstname,
//            "last_name":lastname,
//            "countrycode":countrycodevalue!,
//            ]
//            print(body)
//            apiCall(jsonDic:body)
//        }
    }
//    func apiCall(jsonDic:[String:Any]){
//
//        self.loader()
//        ApiHandler.sharedInstance.callAPIWith(dictionary: jsonDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.REGISTER_METHOD,headerString: "",  onCompletion: {
//                    (json: JSON, err: NSError?) in
//                    if err != nil{
//                        UniversalTaskExecuter.showOKAlertWith(message: err.debugDescription, title: "Error", self)
//                       // print(err!)
//                         return
//                    }
//                    else{
//                        if json["success"].boolValue == true {
//                         self.dismiss(animated: false, completion: nil)
//                            self.firstName.text! = ""
//                            self.lastName.text! = ""
//                            self.email.text! = ""
//                            self.phoneNumber.text! = ""
//                            self.password.text! = ""
//                            self.conformPassword.text! = ""
//                            self.countryCode.setTitle("251", for: .normal)
//                            UniversalTaskExecuter.showOKAlertWith(message: "Register Sucess", title: "Sucess", self)
//                            DispatchQueue.main.async {
//                                self.navigationController?.popViewController(animated: true)
//                            }
//                            print("Data:",json)
//                        }
//                        else{
//                            self.dismiss(animated: false, completion: nil)
//                            UniversalTaskExecuter.showOKAlertWith(message: "\(json["message"])", title: "Error", self)
//                            print("Error")
//                        }
//                    }
//                })
//    }

//    func loader() {
//        let alert = UIAlertController(title: "Regiter User", message: "Please wait...", preferredStyle: .alert)
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = UIActivityIndicatorView.Style.gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
//        self.present(alert, animated: true, completion: nil)
//    }

    func  validtationChecked(name:String,lname:String ,mobileNum:String,email:String,pass:String,cpassword:String,countrycodevalue:String) -> Bool{
        if (name == ""){
            alert(massage:" Please Insert  First Name ")
            self.progressHUD.hide(animated: true)
            return false
          }
        if (lname == ""){
          alert(massage:" Please Insert  Last Name")
            self.progressHUD.hide(animated: true)
          return false
        }
        if (email == ""){
             alert(massage:"Please Insert Email")
            self.progressHUD.hide(animated: true)
            return false
        }

        if email != ""{
            if (!email.isValidEmail){
                alert(massage:" Please Insert Valid Email")
                self.progressHUD.hide(animated: true)
                return false
            }
        }
        if (countrycodevalue == ""){
            alert(massage: " Please Insert Country Code Number")
            self.progressHUD.hide(animated: true)
            return false
        }
        if (mobileNum == ""){
           alert(massage: " Please Insert Mobile Number")
            self.progressHUD.hide(animated: true)
            return false
        }
        if mobileNum != ""{
            if (!mobileNum.isValidPhone){
                alert(massage:" Please Insert Valid Mobile Number")
                self.progressHUD.hide(animated: true)
                return false
            }
        }
        if (pass == ""){
          alert(massage:" Please Insert Password")
            self.progressHUD.hide(animated: true)
          return false
        }
        if pass != ""{
            if (!pass.isValidPassword){
                alert(massage:" Please Insert Password More Then 6 Character")
                self.progressHUD.hide(animated: true)
                return false
            }
        }
        if (cpassword == ""){
          alert(massage:" Please Insert  Conform Password")
            self.progressHUD.hide(animated: true)
          return false
        }
        if (cpassword != pass){
          alert(massage:" Conform Password Not match")
            self.progressHUD.hide(animated: true)
          return false
        }

        return true
    }
    func alert(massage:String){
        let alert = UIAlertController(title: "", message:massage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        present(alert, animated: true)
    }

    @IBAction func gotoLoginPage(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func countryCodeBtn(_ sender: Any){

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CountryCodeViewController") as! CountryCodeViewController
        vc.countrycodeindex = (sender as AnyObject).tag
        vc.delegate = self

        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)

    }
    
    func countrycode(data: [String : Any]){
        countryCode.setTitle(data["value"]! as? String, for: .normal)
        countrycodevalue = data["value"]! as! String
        countryCode.contentHorizontalAlignment = .left
    }

}
extension RegisterViewController: RegisterPresnterToViewProtocol {
    func RegisterSucess() {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            UniversalTaskExecuter.showOKAlertWith(message: "Register Sucess", title: "Sucess", self)
                self.firstName.text! = ""
                self.lastName.text! = ""
                self.email.text! = ""
                self.phoneNumber.text! = ""
                self.password.text! = ""
                self.conformPassword.text! = ""
                self.countryCode.setTitle("251", for: .normal)
        }
    }

    func RegisterError(error: String) {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            UniversalTaskExecuter.showOKAlertWith(message: error, title: "Error", self)
        }
    }

}
