//
//  PasswordChangePresenter.swift
//  SM Driver
//
//  Created by Mac on 21/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation

class PasswordChangePresent: PasswordChangeInteractorToPresenter , PassworChangeViewToPresenter{
    
    
    

    var  interactor : PasswordChangePresenterToInteractorProtocol?
    var view: PasswordChangePresnterToViewProtocol?
    var router :PasswordChangePresenterToRouterProtocol?
    
    func PasswordChange(body:[String:Any]) {
        interactor?.doLoginWith(body:body)
    }
    
    func passwordChangeSucess() {
        view?.ChangePasswordSucess()
        
    }
    
    func passwordChangeFail(error: String) {
        view?.ChangePasswordError(error: error)
    }
}
