//
//  PasswordChangeInterector.swift
//  SM Driver
//
//  Created by Mac on 21/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation
import SwiftyJSON

class PasswordChangeInterector:PasswordChangePresenterToInteractorProtocol{
    
    var presenter: PasswordChangeInteractorToPresenter?

    func doLoginWith(body:[String:Any]){
        print("body:",body)

        if(!Connectivity.isConnectedToInternet){
            presenter?.passwordChangeFail(error: "Internet is not connected")
            return
        }
        
        let inputDic:[String:Any] = body
               
               ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.ChangePassword_METHOD,headerString: "",  onCompletion: {
                   (json: JSON, err: NSError?) in
                   if err != nil{
                       self.presenter?.passwordChangeFail(error: err.debugDescription)
                   }
                   else{
                     //  print(json.stringValue)
                       if json["success"].boolValue == true {
                        print("json:",json["success"])
                           self.presenter?.passwordChangeSucess()
                       }
                       else{
                           let message = json["message"].stringValue
                           self.presenter?.passwordChangeFail(error: message)
                       }
                   }
               })
    }
    

}
