//
//  ChangePasswordViewController.swift
//  SM Driver
//
//  Created by Mac on 17/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD


class ChangePasswordViewController: UIViewController {
    
    
   var passwordchangeDelegate: PassworChangeViewToPresenter?
    
    var progressHUD: MBProgressHUD!
    
    @IBOutlet weak var oldpasswordImg: UIImageView!
    
    @IBOutlet weak var oldpasswordtextfield: UITextField!
    
    @IBOutlet weak var changepasswordOutlet: UIButton!
    @IBOutlet weak var newpasswordtextfield: UITextField!
    @IBOutlet weak var newpasswordImg: UIImageView!
    
    @IBOutlet weak var conpasswordtextfield: UITextField!
    @IBOutlet weak var conpasswordImg: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        changepasswordOutlet.layer.cornerRadius = 25
        //       self.navigationController?.navigationBar.topItem?.title = ""
        self.textfieldplaceholder()
        self.passwordiconimgcolor()
        self.passwordtypehidden()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = "Password Change"
    }
    func passwordtypehidden(){
        oldpasswordtextfield.isSecureTextEntry = true
        newpasswordtextfield.isSecureTextEntry = true
        conpasswordtextfield.isSecureTextEntry = true
    }
    
    func passwordiconimgcolor(){
        oldpasswordImg.tintColor = UIColor.white
        newpasswordImg.tintColor = UIColor.white
        conpasswordImg.tintColor = UIColor.white
    }
    
    func textfieldplaceholder(){
        oldpasswordtextfield.attributedPlaceholder = NSAttributedString(string: "Old Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        newpasswordtextfield.attributedPlaceholder = NSAttributedString(string: "New Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        conpasswordtextfield.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
    }
    
    @IBAction func changepasswordbtnpressed(_ sender: Any) {
       
        self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)
        let oldpassword = oldpasswordtextfield.text
        let newpassword = newpasswordtextfield.text
        let conpassword = conpasswordtextfield.text

        let msg = validtationChecked(oldpassword:oldpassword!,newpassword:newpassword!, conpassword:conpassword!)
        
        if (msg) {
        let body:[String:Any] = [
                   "id": UserDefaultUtils.getUserId(),
                   "old_password":oldpassword!,
                   "new_password":newpassword!,
               ]
//            print("body:",body)
               passwordchangeDelegate?.PasswordChange(body: body)
        }
//        if msg{
//            self.changepasswordAPI(oldpassword:oldpassword!,newpassword:newpassword!)
//        }
    }
    
    func validtationChecked(oldpassword:String,newpassword:String,conpassword:String) -> Bool {
        
        if (oldpassword == ""){
            self.progressHUD.hide(animated: true)
            alert(massage:" Please Insert Old Password")
            return false
        }
        if (newpassword == ""){
            self.progressHUD.hide(animated: true)
            alert(massage:" Please Insert New Password")
            return false
        }
        if newpassword != ""{
            self.progressHUD.hide(animated: true)
            if (!newpassword.isValidPassword){
                alert(massage:" Please Insert Password More Then 6 Character")
                return false
            }
        }
        if (conpassword == ""){
            self.progressHUD.hide(animated: true)
            alert(massage:" Please Insert  Conform Password")
            return false
        }
        if (conpassword != newpassword){
            self.progressHUD.hide(animated: true)
            alert(massage:" Conform Password Not match")
            return false
        }
        
        return true
        
    }
    func alert(massage:String){
        let alert = UIAlertController(title: "", message:massage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
//    func changepasswordAPI(oldpassword:String,newpassword:String){
//        if(!Connectivity.isConnectedToInternet){
//            UniversalTaskExecuter.showOKAlertWith(message: "Internet is not connected", title: "Error", self)
//            return
//        }
//        let body:[String:Any] = [
//            "id": UserDefaultUtils.getUserId(),
//            "old_password":oldpassword,
//            "new_password":newpassword,
//        ]
//        print(body)
//        ApiHandler.sharedInstance.callAPIWith(dictionary: body, url: Constants.BASE_URL + Constants.API_VERSION+Constants.ChangePassword_METHOD,headerString: "",  onCompletion: {
//            (json: JSON, err: NSError?) in
//            if err != nil{
//                self.progressHUD.hide(animated: true)
//                UniversalTaskExecuter.showOKAlertWith(message: "\(err.debugDescription)", title: "Error", self)
//               // print(err)
//                return
//            }
//            else{
//                self.progressHUD.hide(animated: true)
//                if json["success"].boolValue == true {
//                    print("json success:",json)
//                    UniversalTaskExecuter.showOKAlertWith(message: "Password Change", title: "", self)
//                }
//                else{
//                    self.progressHUD.hide(animated: true)
//                    print(json["message"])
//                    UniversalTaskExecuter.showOKAlertWith(message: "\(json["message"])", title: "Error", self)
//                    print("Error")
//                }
//            }
//        })
//    }
    
}

extension ChangePasswordViewController : PasswordChangePresnterToViewProtocol{
   
    func ChangePasswordSucess() {
        DispatchQueue.main.async{
            self.progressHUD.hide(animated: true)
            UniversalTaskExecuter.showOKAlertWith(message: "Password Change Sucess", title: "Sucess", self)
        }
    }
    
    func ChangePasswordError(error: String) {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            UniversalTaskExecuter.showOKAlertWith(message: error, title: "Error", self)
        }
    }
}
