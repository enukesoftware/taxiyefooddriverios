//
//  ChangePasswordPresenterToInteractorProtocol.swift
//  SM Driver
//
//  Created by Mac on 21/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation

protocol PasswordChangePresenterToInteractorProtocol : class {
    func doLoginWith(body:[String:Any])
}
