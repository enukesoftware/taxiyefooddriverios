//
//  PasswordChangePresnterToViewProtocol.swift
//  SM Driver
//
//  Created by Mac on 21/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation

protocol PasswordChangePresnterToViewProtocol: class {
    func ChangePasswordSucess()
    func ChangePasswordError(error:String)
}
