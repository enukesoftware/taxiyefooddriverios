//
//  PasswordChangePresenterToRouterProtocol.swift
//  SM Driver
//
//  Created by Mac on 24/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation
protocol PasswordChangePresenterToRouterProtocol : class {
    
    static func createModule()-> ChangePasswordViewController
    
}
