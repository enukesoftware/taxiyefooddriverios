//
//  PasswordChangeRouter.swift
//  SM Driver
//
//  Created by Mac on 24/02/20.
//  Copyright © 2020 mac-2. All rights reserved.
//

import Foundation
import UIKit

class PasswordChangeRouter:PasswordChangePresenterToRouterProtocol {

    static func createModule() -> ChangePasswordViewController {
            let view = mainstoryboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        let presenter: PasswordChangePresent = PasswordChangePresent()
        let interactor: PasswordChangeInterector = PasswordChangeInterector()
        let router:PasswordChangeRouter = PasswordChangeRouter()
        
        view.passwordchangeDelegate = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
}
