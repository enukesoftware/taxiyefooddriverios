//
//  CompleteOrderInteractor.swift
//  SM Driver
//
//  Created by mac-2 on 13/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CompleteOrderInteractor : CompleteOrderPresnterToInteractor {
    
    var presenter : CompleteOrderInteractorToPresenter?
    
    func fetchDataFromServer(from:String,to:String) {
            
            if(!Connectivity.isConnectedToInternet){
                presenter?.showError(error: "Internet is not connected")
                return
            }
        
         let inputDic:[String:Any]?
        if from != ""
        {
            inputDic  = ["status":"5","driver_id":UserDefaultUtils.getUserId(),"date_from":from,"date_to":to]
        }
        else
        {
         inputDic = ["status":"5","driver_id":UserDefaultUtils.getUserId()]
        }
            
        ApiHandler.sharedInstance.makeHTTPPostRequestWithData(path: Constants.BASE_URL + Constants.API_VERSION+Constants.COMPLETE_ORDERS, body: inputDic as! [String : AnyObject], headerString: "", onCompletion: { data , err in
            
           // onCompletion(json as JSON, err)
            
            if err != nil{
                self.presenter?.showError(error: err.debugDescription)
            }
            else{
                //                    print(json.stringValue)
                //                    if json["success"].boolValue == true {
                //                        self.presenter?.completeOrderList()
                //
                //                    }
                //                    else{
                //                        let message = json["message"].stringValue
                //                        self.presenter?.showError(error: message)
                //                    }
                let orders = try? JSONDecoder().decode(CompleteOrder.self, from: data!)
                
                if let order = orders {
                    if (!order.success){
                        self.presenter?.showError(error: "Server Error")
                    }
                    else {
                        self.presenter?.completeOrderList(orders: order.data)
                    }
                }
                }
        })
    }
        
}
