//
//  CompleteOrderPresenter.swift
//  SM Driver
//
//  Created by mac-2 on 13/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation

class CompleteOrederPresnter : CompleteOrderViewToPresenter, CompleteOrderInteractorToPresenter {
    
    var view : CompleteOrderPresenterToView?
    var interactor: CompleteOrderPresnterToInteractor?
    var router : CompleteOrderPresenterToRouter?
    
    
    func fetchOrderListData(from:String,to:String)
    {
        interactor?.fetchDataFromServer(from:from,to:to)
    }
    
    func completeOrderList(orders: [CompleteOrderData]) {
        view?.updateOrderTable(orders: orders)
    }
    
    func showError(error: String) {
        view?.updateError(error: error)
    }
    
    
}
