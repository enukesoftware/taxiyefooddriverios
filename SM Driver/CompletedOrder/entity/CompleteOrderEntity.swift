//
//  CompleteOrderEntity.swift
//  SM Driver
//
//  Created by mac-2 on 13/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//
//   let order = try? newJSONDecoder().decode(Login.self, from: jsonData)

// MARK: - CompleteOrder
struct CompleteOrder: Codable {
    let success: Bool
    let data: [CompleteOrderData]
}

// MARK: - Datum
struct CompleteOrderData: Codable {
    let shipJSON: ShipJSON
    let id, driverID, date, time: String
    let asap: String?
    let amount, discount, orderType, shippingCharge: String
    let orderNumber, status: String
    let remark: String?
    let createdAt, shipAdd1, shipAdd2, shipCity: String
    let shipZip, shipMobile, shipLat, shipLong: String
    let firstName, lastName, contactNumber: String
    let couponCode, couponValue: JSONNull?
    let restaurantName, restaurantLongitude, restaurantLatitude, restaurantRating: String
    let restaurantFloor, restaurantStreet: String?
    let restaurantCompany: JSONNull?
    let restaurantArea, restaurantCity: String?
    
    enum CodingKeys: String, CodingKey {
        case shipJSON = "ship_json"
        case id
        case driverID = "driver_id"
        case date, time, asap, amount, discount
        case orderType = "order_type"
        case shippingCharge = "shipping_charge"
        case orderNumber = "order_number"
        case status, remark
        case createdAt = "created_at"
        case shipAdd1 = "ship_add1"
        case shipAdd2 = "ship_add2"
        case shipCity = "ship_city"
        case shipZip = "ship_zip"
        case shipMobile = "ship_mobile"
        case shipLat = "ship_lat"
        case shipLong = "ship_long"
        case firstName = "first_name"
        case lastName = "last_name"
        case contactNumber = "contact_number"
        case couponCode = "coupon_code"
        case couponValue = "coupon_value"
        case restaurantName = "restaurant_name"
        case restaurantLongitude = "restaurant_longitude"
        case restaurantLatitude = "restaurant_latitude"
        case restaurantRating = "restaurant_rating"
        case restaurantFloor = "restaurant_floor"
        case restaurantStreet = "restaurant_street"
        case restaurantCompany = "restaurant_company"
        case restaurantArea = "restaurant_area"
        case restaurantCity = "restaurant_city"
    }
}

// MARK: - ShipJSON
struct ShipJSON: Codable {
    let id : String?
    let user_id : String?
    let first_address : String?
    let second_address : String?
    let city_id : String?
    let state_id : String?
    let zip : String?
    let landmark : String?
    let country_id : String?
    let area_id : String?
    let address_json : AddressJSON?
    let longitude : String?
    let latitude : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case first_address = "first_address"
        case second_address = "second_address"
        case city_id = "city_id"
        case state_id = "state_id"
        case zip = "zip"
        case landmark = "landmark"
        case country_id = "country_id"
        case area_id = "area_id"
        case address_json = "address_json"
        case longitude = "longitude"
        case latitude = "latitude"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        first_address = try values.decodeIfPresent(String.self, forKey: .first_address)
        second_address = try values.decodeIfPresent(String.self, forKey: .second_address)
        city_id = try values.decodeIfPresent(String.self, forKey: .city_id)
        state_id = try values.decodeIfPresent(String.self, forKey: .state_id)
        zip = try values.decodeIfPresent(String.self, forKey: .zip)
        landmark = try values.decodeIfPresent(String.self, forKey: .landmark)
        country_id = try values.decodeIfPresent(String.self, forKey: .country_id)
        area_id = try values.decodeIfPresent(String.self, forKey: .area_id)
        address_json = try values.decodeIfPresent(AddressJSON.self, forKey: .address_json)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
    }
}

// MARK: - AddressJSON
struct AddressJSON: Codable {
    let area_name : String?
    let city : String?
    let floor_unit : String?
    let house_no : String?
    let state_id : String?
    let street : String?
    
    enum CodingKeys: String, CodingKey {
        
        case area_name = "area_name"
        case city = "city"
        case floor_unit = "floor_unit"
        case house_no = "house_no"
        case state_id = "state_id"
        case street = "street"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        area_name = try values.decodeIfPresent(String.self, forKey: .area_name)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        floor_unit = try values.decodeIfPresent(String.self, forKey: .floor_unit)
        house_no = try values.decodeIfPresent(String.self, forKey: .house_no)
        state_id = try values.decodeIfPresent(String.self, forKey: .state_id)
        street = try values.decodeIfPresent(String.self, forKey: .street)
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
