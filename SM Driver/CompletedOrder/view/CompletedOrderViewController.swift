//
//  CompletedOrderViewController.swift
//  SM Driver
//
//  Created by mac-2 on 12/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit
import MBProgressHUD

class CompletedOrderViewController: UIViewController , CompleteOrderPresenterToView,CompleteOrderFilters {
    
     var progressHUD: MBProgressHUD!
    
    var presenter : CompleteOrederPresnter?
    
     var orderList = [CompleteOrderData]()
    
    var filetereddata:[CompleteOrderData]?
    
    var currentdetailToView : CompleteOrderData?
    
    var isFilterApplied:Bool?
    
    @IBOutlet weak var completeTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Completed Order"
        
        setRightBarButton()
        
        completeTableView.tableFooterView = UIView(frame: .zero)
        
        self.progressHUD = MBProgressHUD.showAdded(to: self.completeTableView , animated: true)
        
        presenter?.fetchOrderListData(from: "", to: "")
        completeTableView.register(UINib(nibName: "OrderIdTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderIdTableViewCell")
        completeTableView.register(UINib(nibName: "OrderAddressTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderAddressTableViewCell")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func setRightBarButton(){
        
        // Right navigation items
        
        let rightBarButtonActive = UIBarButtonItem(image: UIImage(named: "filter_icon")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.onFilterButtonClick))
        rightBarButtonActive.tag = 1
        self.navigationItem.rightBarButtonItems = [ rightBarButtonActive ]
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc private func onFilterButtonClick(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompleteOrderFilter") as? CompleteOrderFilter
        vc!.delegate = self
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    func getfilerDate(from:String,to:String)
    {
         presenter?.fetchOrderListData(from: from, to:to)
        
//    {  let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MMM-dd"
//     filetereddata =     orderList.filter{
//
//            self.isBetween(dateFormatter.date(from: from)!, and: dateFormatter.date(from: to)!, and: $0.date)
//        }
//        isFilterApplied = true
//        completeTableView.reloadData()
    
        
    }
    
    func isBetween(_ date1: Date, and date2: Date,and comparisonDate:String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let comparisondateform = dateFormatter.date(from: comparisonDate)!
        
        return (min(date1, date2) ... max(date1, date2)).contains(comparisondateform)
    }

}




extension CompletedOrderViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if currentdetailToView != nil
        {
             return 3
        }
        else
        {
        if isFilterApplied != nil && isFilterApplied!
        {
            return filetereddata!.count
        }else
        {
        return orderList.count
        }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if currentdetailToView != nil
        {
            
            
            switch(indexPath.row)
            {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "OrderIdTableViewCell", for: indexPath) as! OrderIdTableViewCell
                if let id = currentdetailToView?.orderNumber ,let amount = currentdetailToView?.amount
                {
                    cell.setValueToLabel(orderid: id, amount: amount)
                }
                
                cell.heightCancelButton.constant = 0
                cell.statusLabel.text = "Delivered"
                
                return cell
                
                
            case 1:
                let  cell = tableView.dequeueReusableCell(withIdentifier: "OrderAddressTableViewCell", for: indexPath) as! OrderAddressTableViewCell
                
                
                    cell.setValueInLabel(type: "pickup", locationName:getPickupAddress(order: currentdetailToView!) )
                    if let resturantName = currentdetailToView?.restaurantName
                    {
                        cell.locationNamelabel.text = resturantName
                    }
                    
               
                
                cell.mapButton.tag = 1
//                cell.mapButton.addTarget(self, action: (#selector(openGooglemaps(sender:))), for: .touchUpInside)
                
                return cell
                
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "OrderAddressTableViewCell", for: indexPath) as! OrderAddressTableViewCell
               
                cell.setValueInLabel(type: "", locationName: getDeliverAddress(order: currentdetailToView!))
                    
                    if let dropName = currentdetailToView?.shipJSON.address_json?.area_name
                    {
                        cell.locationNamelabel.text = dropName
                    }
                
                cell.mapButton.tag = 2
//                cell.mapButton.addTarget(self, action: (#selector(openGooglemaps(sender:))), for: .touchUpInside)
                return cell
                
                
            default:
                return UITableViewCell()
            }
            
            
        }
        else
        {
         let tableCell = tableView.dequeueReusableCell(withIdentifier: "CompleteOrderTableViewCell", for: indexPath) as! CompleteOrderTableViewCell
        
        tableCell.selectionStyle = UITableViewCell.SelectionStyle.none

        var  current:CompleteOrderData
        if isFilterApplied != nil && isFilterApplied!
        {
            current = filetereddata![indexPath.row]
        }
        else
        {
         current = orderList[indexPath.row]
        }
        tableCell.deliveredTo.text = current.firstName+current.lastName
        tableCell.orderId.text = current.orderNumber
        tableCell.amount.text = "$"+current.amount
        tableCell.date.text = current.date + current.time
        
        return tableCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if currentdetailToView != nil
        {
            
                        var height = 300
            
                        switch(indexPath.row)
                        {
                        case 0:
                            height =  86
            
                        break
                        case 1:
                            height =  120
            
                            break
            
                        case 2:
                            height =  120
            
                            break
            
                        default:
                           height = 300
                       }
                        return CGFloat(height)
        }
        else
        {
        return 120
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentdetailToView = orderList[indexPath.row]
        self.completeTableView.reloadData()
        
    }
    
    func updateOrderTable(orders: [CompleteOrderData]) {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            self.orderList = orders;
            self.completeTableView.reloadData()
        }
    }
    
    func updateError(error: String) {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
        UniversalTaskExecuter.showOKAlertWith(message: error, title: "Error", self)
        }
    }
    
    func getPickupAddress(order:CompleteOrderData) -> String
    {
        var pickupAddress:String = ""
        pickupAddress = order.restaurantFloor == "" ? "": order.restaurantFloor ?? " " + ", "
        
        if let restaurantStreet = order.restaurantStreet{
            if restaurantStreet != ""{
                pickupAddress = pickupAddress + restaurantStreet + ", "
            }
        }
        
        if let restaurantArea = order.restaurantArea{
            if restaurantArea != ""{
                pickupAddress = pickupAddress + restaurantArea + ", "
            }
        }
        
        if let restaurantCity = order.restaurantCity{
            if restaurantCity != ""{
                pickupAddress = pickupAddress + restaurantCity
            }
        }
        
        return pickupAddress
    }
    
    func getDeliverAddress(order:CompleteOrderData) -> String
    {
        var deliverAddress:String = ""
        if let floor = order.shipJSON.address_json?.floor_unit{
            if floor != ""{
                deliverAddress = deliverAddress+floor+", "
            }
        }
        
        if let houseNo = order.shipJSON.address_json?.house_no {
            if houseNo != ""{
                deliverAddress = deliverAddress + houseNo + ", "
            }
        }
        
        if let street = order.shipJSON.address_json?.street {
            if street != "" {
                deliverAddress = deliverAddress + street + ", "
            }
        }
        
        if let areaname = order.shipJSON.address_json?.area_name {
            if areaname != "" {
                deliverAddress = deliverAddress + areaname + ", "
            }
        }
        
        if let city = order.shipJSON.address_json?.city{
            if city != "" {
                deliverAddress = deliverAddress + city
            }
        }
        return deliverAddress
    }

    
}
