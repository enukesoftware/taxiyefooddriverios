//
//  CompleteOrderFilter.swift
//  SM Driver
//
//  Created by mac-2 on 14/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class CompleteOrderFilter: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var date_from_field: UITextField!
    @IBOutlet weak var date_to_field: UITextField!
    var delegate : CompleteOrderFilters?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
        if textField.tag == 1
        {
            
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            textField.inputView = datePickerView
            datePickerView.tag = 1
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            textField.text = Date().getString(format: "yyyy-MM-dd")
        }else
        {
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            textField.inputView = datePickerView
             datePickerView.tag = 2
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            textField.text = Date().getString(format: "yyyy-MM-dd")

        }
    }
    
    
    
    
    @objc func handleDatePicker(sender: UIDatePicker) {
       
        if sender.tag == 1
        {
        date_from_field.text = sender.date.getString(format: "yyyy-MM-dd")
        }else
        {
            date_to_field.text = sender.date.getString(format: "yyyy-MM-dd")

        }
        
    }
   
    
    func checkValidationForFilter() -> Bool
    {
        if date_from_field.text?.count == 0
        {
            validationAlert(alert: "Date from field cannot be empty")
            return false
        }
        if date_to_field.text?.count == 0
        {
            validationAlert(alert: "Date to field cannot be empty")
            return false
        }
        
        if  date_from_field.text!.getdate(format: "yyyy-MM-dd") > date_to_field.text!.getdate(format: "yyyy-MM-dd")
        {
            validationAlert(alert: "Date from cannot be greater than Date To")

            return false
        }
        
        return true
        
    }
    
    
    func validationAlert(alert:String)
    {
        let alertController = UIAlertController(title: alert, message: "", preferredStyle: UIAlertController.Style.alert)
        
        let saveAction = UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { alert -> Void in
            
            
        })
        
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)

        
    }
    
    
    
    
    
    
    

    @IBAction func date_apply_action(_ sender: Any) {
        if checkValidationForFilter() == false
        {
            return
        }
        
        delegate?.getfilerDate(from:  date_from_field.text!, to:  date_to_field.text!)
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
