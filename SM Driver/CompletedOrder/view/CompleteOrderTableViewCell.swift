//
//  CompleteOrderTableViewCell.swift
//  SM Driver
//
//  Created by mac-2 on 12/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

class CompleteOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var deliveredTo: UILabel!
    @IBOutlet weak var orderId: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
