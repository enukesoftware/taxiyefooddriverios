//
//  CompleteOrderRouter.swift
//  SM Driver
//
//  Created by mac-2 on 13/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit

class CompleteOrderRouter : CompleteOrderPresenterToRouter {
    
    
    func moveToNextScreen() {
        
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    static func createModule() -> CompletedOrderViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "CompletedOrderViewController") as! CompletedOrderViewController
        
        let presenter: CompleteOrederPresnter = CompleteOrederPresnter()
        let interactor: CompleteOrderInteractor = CompleteOrderInteractor()
        let router:CompleteOrderRouter = CompleteOrderRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
}
