//
//  CompleteOrderProtocols.swift
//  SM Driver
//
//  Created by mac-2 on 12/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation

protocol CompleteOrderViewToPresenter {
    func fetchOrderListData(from:String,to:String)
}
protocol CompleteOrderPresenterToView {
    func updateOrderTable(orders:[CompleteOrderData])
    func updateError(error : String)
}

protocol CompleteOrderPresnterToInteractor {
    func fetchDataFromServer(from:String,to:String)
}
protocol CompleteOrderInteractorToPresenter {
    func completeOrderList(orders:[CompleteOrderData])
    func showError(error : String)
}
protocol CompleteOrderPresenterToRouter {
    func moveToNextScreen()
}

protocol CompleteOrderFilters {
    func getfilerDate(from:String,to:String)
}
