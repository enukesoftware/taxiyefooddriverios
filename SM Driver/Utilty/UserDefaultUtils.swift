//
//  UserDefaultUtils.swift
//  SM Driver
//
//  Created by mac-2 on 31/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation

public  enum OrderDictionaryenum {
    case ordernumber
    case orderAmount
    case orderPickupAddress
    case orderDeliveryAddress
    case resturantName
    case pickupLat
    case pickupLong
    case deliveryLat
    case deliveryLong
    case deliveryName
    
    case orderUserName
    case orderUserContact
    case CustomerImg
  
    var instance: String {
        switch self {
        case .ordernumber: return "order_number"
        case .orderAmount: return "order_Amount"
        case .orderPickupAddress: return "pickupAddress"
        case .orderDeliveryAddress: return "deliveryAddress"
        case .resturantName : return "resturant_Name"
        case .pickupLat: return"pickup_lat"
        case .pickupLong : return"pickup_long"
        case .deliveryLat : return "delivery_lat"
        case .deliveryLong : return "delivery_long"
        case .deliveryName : return "delivery_Name"
        
        case .orderUserName:  return "first_name"
        case .orderUserContact: return "contact_number"
        
        case .CustomerImg: return "profile_image"
            
        }
    }

}


class UserDefaultUtils {
    
    static let LoggedIn = "LoggedIn"
    static let UserId = "UserId"
    static let UserName = "UserName"
    static let UserEmail = "UserEmail"
    static let OrderDict = "orderDict"
    static let Orderid = "orderid"
    static let currentOrderStatus = "currentOrderStatus"

    static let ProfileImage = "ProfileImage"
    static let phoneNum = "phoneNum"
    static let countrycode = "countrycode"
    static let Fname = "fname"
    static let Lname = "lname"
    
    static func setCurrrentOrderStatus(status: String)
    {
        UserDefaults.standard.set(status, forKey: currentOrderStatus)

    }
    
    static func getCurrentOrderStatus()->String?
    {
        return UserDefaults.standard.string(forKey: currentOrderStatus)

    }
   
    
    static func setLoggedIn(isLogin: Bool){
        UserDefaults.standard.set(isLogin, forKey: LoggedIn)
    }
    
    static func isLoggedIn() -> Bool {
       return UserDefaults.standard.bool(forKey: LoggedIn)
    }
    
    static func setUserId(userId : String) {
        UserDefaults.standard.set(userId, forKey: UserId)
    }
    
    static func getUserId() -> String {
        return UserDefaults.standard.string(forKey: UserId) ?? ""
    }
    
    static func setorderId(orderId : String) {
        UserDefaults.standard.set(orderId, forKey: Orderid)
    }
    
    static func getOrderid() -> String {
        return UserDefaults.standard.string(forKey: Orderid) ?? ""
    }
    
    
    static func setDriverActiveStatus(status:Bool){
        UserDefaults.standard.set(status, forKey: "UserStatus")
    }
    
    static func getDriverActiveStatus() -> Bool{
        return UserDefaults.standard.bool(forKey: "UserStatus")
    }
    static func setUserName(username : String) {
        UserDefaults.standard.set(username, forKey: UserName)
    }
    
    static func getUserName() -> String {
        return UserDefaults.standard.string(forKey: UserName) ?? ""
    }
    
    static func setFname(fname : String) {
        UserDefaults.standard.set(fname, forKey: Fname)
    }
    static func getFname() -> String {
        return UserDefaults.standard.string(forKey: Fname) ?? ""
    }
    
    static func setLname(lname : String) {
        UserDefaults.standard.set(lname, forKey: Lname)
    }
    static func getLname() -> String {
        return UserDefaults.standard.string(forKey: Lname) ?? ""
    }
    
    static func setUserEmail(useremail : String) {
        UserDefaults.standard.set(useremail, forKey: UserEmail)
    }
    
    static func getUserEmail() -> String {
        return UserDefaults.standard.string(forKey: UserEmail) ?? ""
    }
    
    static func setProfileImageURL(image: String){
        UserDefaults.standard.set(image, forKey: ProfileImage)

    }
    static func getProfileImageURL() -> String{
//return  UserDefaults.standard.url(forKey: ProfileImage)//
       return UserDefaults.standard.string(forKey: ProfileImage) ?? ""
    }
    
    static func setContactNum(contact:String){
        UserDefaults.standard.set(contact,forKey: phoneNum)
    }
    static func getContactNum() -> String{
           return UserDefaults.standard.string(forKey: phoneNum) ?? ""
        }
    static func setCountryCode(country:String){
        UserDefaults.standard.set(country,forKey: countrycode)
    }
    static func getCountryCode() -> String{
           return UserDefaults.standard.string(forKey: countrycode) ?? ""
        }
    static func setOrderDictionary(orderdict: Dictionary<String,Any>)  {
         UserDefaults.standard.set(orderdict, forKey: OrderDict)
    }
    
    static func getOrderDictionary() -> Dictionary<String,Any>?  {
        return UserDefaults.standard.dictionary(forKey: OrderDict)
    }
    
    static func setnilOrderDictionary()
    {
        UserDefaults.standard.set(nil, forKey: OrderDict)

    }
    
    
}
