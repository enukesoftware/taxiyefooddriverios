//
//  Constants.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit

struct Constants  {
    
    static let BASE_URL : String = "http://fd.yiipro.com"
    static let API_VERSION : String = "/en/api/v1"
    static let LOGIN_METHOD : String = "/driver/signin"
    static let UPDATE_STATUS : String = "/driver/Update"
    static let COMPLETE_ORDERS : String = "/driver/OrderByDriverId"
    static let GET_ORDERS : String = "/driver/OrderList"
    static let UPDATE_ORDER : String = "/driver/OrderUpdate"
    static let UPDATE_PROFILE : String = "/driver/update"
    static let REGISTER_METHOD : String = "/driver/signup"
    static let ChangePassword_METHOD : String = "/driver/changepassword"
    static let Device_Token_METHOD : String = "/update-device-token"
    

    
    static let DEACTIVE_STATUS : String = "You are deactive, Please active by clicking active button at top right corner"
    
    struct appDelegateConstant {
        
        static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    static var firebasesendData:SendLocationFirebase?
}
