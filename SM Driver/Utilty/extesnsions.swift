//
//  extesnsions.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String
{
    func getdate(format:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)!
    }
}


extension UIButton
{
    func setCornerradius(amount :CGFloat)  {
        self.layer.cornerRadius = amount
        self.clipsToBounds = true
        
    }
    
    func setBorder(radius:CGFloat,color:UIColor)
    {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
    }
    
    
}

extension UIView
{
    func setCornerradiusView(amount :CGFloat)  {
        self.layer.cornerRadius = amount
        self.clipsToBounds = true
        
    }
}

extension Date
{
    func getString(format:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
