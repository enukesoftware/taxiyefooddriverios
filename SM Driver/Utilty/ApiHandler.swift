//
//  ApiHandler.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import SwiftyJSON
import MBProgressHUD

typealias ServiceResponse = (JSON, NSError?) -> Void
typealias CompleteHandlerBlock = () -> ()

class ApiHandler: NSObject {
    
    var totalHitAPIs : Int = 0
    var successAPIs : Int = 0
    var successPercent : Int64 = 0
    
    static let sharedInstance = {ApiHandler()}()
    
    var timer : Timer?
    
    
    
    
    // MARK: Perform a GET Request
    private func makeHTTPGetRequest(path: String, onCompletion: @escaping ServiceResponse) -> URLSessionDataTask {
       
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        let session = URLSession.shared
        self.totalHitAPIs += 1
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if let jsonData = data {
                self.successAPIs += 1
                do {
                    let json:JSON = try JSON(data: jsonData)
                    onCompletion(json, nil)
                }
                catch let error {
                    print("error occured \(error)")
                }
            } else {
                onCompletion(JSON.null, error as NSError?)
            }
        })
        
        task.resume()
        return task
    }
    
    // MARK: Perform a POST Request
    private func makeHTTPPostRequest(path: String, body: [String: AnyObject]?, headerString: String, onCompletion: @escaping ServiceResponse) -> URLSessionDataTask {
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        // Set the method to POST
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let loginData = headerString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        
        do {
            // Set the POST body for the request
            
            if let _ = body {
                let jsonBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
                
                let jsonLength = String(jsonBody.count)
                request.setValue(jsonLength, forHTTPHeaderField: "Content-Length")
                
                request.httpBody = jsonBody
            }
            let configuration = URLSessionConfiguration.default
            
            //            let configuration = URLSessionConfiguration.background(withIdentifier: "com.EnukeSoftware.Yota")
            //            configuration.isDiscretionary = true
            configuration.timeoutIntervalForRequest = TimeInterval(180)
            configuration.timeoutIntervalForResource = TimeInterval(180)
            
            let session = URLSession(configuration: configuration)
            self.totalHitAPIs += 1
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response ,error -> Void in
                
                if let jsonData = data {
                    self.successAPIs += 1
                    DispatchQueue.main.async {
                        do {
                            
                            let json:JSON = try JSON(data: jsonData)
                            onCompletion(json, nil)
                        } catch let error {
                            print("error occured \(error)")
                        }
                    }
                } else {
                    
                    let userInFoError = error as! NSError
                    if error.unsafelyUnwrapped.localizedDescription.lowercased() != "cancelled"{
                        
                    }
                    onCompletion(JSON.null, error as NSError?)
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
            return task
        } catch {
            // Create your personal error
            onCompletion(JSON.null, nil)
        }
        return URLSessionDataTask.init()
    }
    
    
    
    
    // MARK: Perform a POST Request
     func makeHTTPPostRequestWithData(path: String, body: [String: AnyObject]?, headerString: String, onCompletion: @escaping (Data?, NSError?) -> Void) -> URLSessionDataTask {
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        // Set the method to POST
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let loginData = headerString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        
        do {
            // Set the POST body for the request
            
            if let _ = body {
                let jsonBody = try JSONSerialization.data(withJSONObject: body!, options: .prettyPrinted)
        
                let jsonLength = String(jsonBody.count)
                request.setValue(jsonLength, forHTTPHeaderField: "Content-Length")
                
                request.httpBody = jsonBody
            }
            let configuration = URLSessionConfiguration.default
            
            //            let configuration = URLSessionConfiguration.background(withIdentifier: "com.EnukeSoftware.Yota")
            //            configuration.isDiscretionary = true
            configuration.timeoutIntervalForRequest = TimeInterval(180)
            configuration.timeoutIntervalForResource = TimeInterval(180)
            
            let session = URLSession(configuration: configuration)
            self.totalHitAPIs += 1
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response ,error -> Void in
                
                if let jsonData = data {
                    self.successAPIs += 1
                    DispatchQueue.main.async {
                        onCompletion(jsonData, nil)
                    }
                } else {
                    
                    _ = error! as NSError
                    if error.unsafelyUnwrapped.localizedDescription.lowercased() != "cancelled"{
                        
                    }
                    onCompletion(nil, error as NSError?)
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
            return task
        } catch {
            // Create your personal error
            onCompletion(nil, nil)
        }
        return URLSessionDataTask.init()
    }
    
    func callAPIWith(dictionary: Dictionary<String,String>,url: String,headerString : String, onCompletion: @escaping (JSON, NSError?) -> Void) {
        
        makeHTTPPostRequest(path: url, body: dictionary as [String : AnyObject], headerString: headerString, onCompletion: { json, err in
            
            onCompletion(json as JSON, err)
            
        })
    }
    
    
    func callAPIWith(dictionary: Dictionary<String,Any>?,url: String, headerString : String, onCompletion: @escaping (JSON ,NSError?) -> Void) {
        
        let task = makeHTTPPostRequest(path: url, body: dictionary as [String : AnyObject]?, headerString: headerString, onCompletion: { json, err in
            onCompletion(json as JSON, err)
        })
        
    }
    
    
    func callGetAPIWith(url: String, onCompletion: @escaping (JSON) -> Void) {
        let task = makeHTTPGetRequest(path: url, onCompletion: { json, err in
            
            onCompletion(json as JSON)
            
        })
    }
    
    func callApiWithTask(dictionary: Dictionary<String,Any>?,url: String, headerString : String, onCompletion: @escaping (JSON ,NSError?)->()) -> URLSessionDataTask {
        
        let task = makeHTTPPostRequest(path: url, body: dictionary as [String : AnyObject]?, headerString: headerString, onCompletion: { json, err in
            onCompletion(json as JSON, err)
        })
        return task
    }
    
    func getAppState() -> String {
        
        var string = ""
        let state = UIApplication.shared.applicationState
        if state == .background {
            print("App in Background")
            string = "App in Background State"
        }
        else if state == .active {
            print("App in active")
            string = "App in Active State"
        }
        else if state == .inactive {
            print("App in inactive")
            string = "App in Inactive State"
        }
        return string
    }
    
    func getcurrentTime() -> String {
        
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MMM.yyyy"
        let currentDate = formatter.string(from:date)
        let string = "Date:\(currentDate) | Time = \(hour):\(minutes):\(seconds)"
        print("hours = \(hour):\(minutes):\(seconds)")
        return string
        
    }
    
}


