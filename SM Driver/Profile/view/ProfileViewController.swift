////
////  ProfileViewController.swift
////  SM Driver
////
////  Created by Mac on 23/01/20.
////  Copyright © 2020 mac-2. All rights reserved.
////


import UIKit
import SwiftyJSON
import AVFoundation
import Foundation
import MobileCoreServices
import Photos



class ProfileViewController: UIViewController,UNUserNotificationCenterDelegate,CountryCodeViewControllerDelegate {
    
    
    
    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var firstnameImg: UIImageView!
    @IBOutlet weak var lastnameImg: UIImageView!
    @IBOutlet weak var emailImg: UIImageView!
    @IBOutlet weak var phoneImg: UIImageView!
    
    var countrycodevalue:String? = ""
    var imagePicker = UIImagePickerController()
    var drive_Id: String?
    @IBOutlet weak var userImgBtn: UIButton!
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var userLastName: UITextField!
    @IBOutlet weak var userFirstName: UITextField!
    
    @IBOutlet weak var userEmail: UITextField!
    
    @IBOutlet weak var userPhoneNum: UITextField!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var countryCodeBtnOutlet: UIButton!
    
    @IBOutlet weak var updateProfile: UIButton!
    
    var profileImage : NSString? = ""
    //    var profileName:String? = ""
    
    @IBOutlet weak var cameraIcon: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.driverProfile()
        self.iconImgColor()
        
        
        updateProfile.layer.cornerRadius = 25
        
        cameraIcon.imageEdgeInsets = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
        cameraIcon.layer.cornerRadius = 15
        cameraIcon.layer.masksToBounds = true
        
        
        
        imagePicker.delegate = self
        userImg.layer.cornerRadius = 50
        userImg.layer.masksToBounds = true
        
        
        
        self.navigationController?.navigationBar.topItem?.title = ""
        countryCodeBtnOutlet.contentHorizontalAlignment = .left
        
        self.userPhoneNum.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = "Profile"
    }
    
    func iconImgColor(){
        firstnameImg.tintColor = UIColor.white
        lastnameImg.tintColor = UIColor.white
        countryImg.tintColor = UIColor.white
        emailImg.tintColor = UIColor.white
        phoneImg.tintColor = UIColor.white
        cameraIcon.tintColor = UIColor.white
    }
    
    //    Country Code Select
    func countrycode(data: [String : Any]) {
        countryCodeBtnOutlet.setTitle(data["value"]! as? String, for: .normal)
        countrycodevalue = data["value"]! as! String
        countryCodeBtnOutlet.contentHorizontalAlignment = .left
    }
    
    
    
    //    func checkPermission() {
    //        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    //        switch authStatus {
    //        case .authorized:
    //            self.displayUploadImageDialog(btnSelected: self.userImgBtn)
    //        case .denied:
    //            print("Error")
    //        default:
    //            break
    //        }
    //    }
    //
    //
    //    func checkLibrary() {
    //        let photos = PHPhotoLibrary.authorizationStatus()
    //        if photos == .authorized {
    //            switch photos {
    //            case .authorized:
    //                self.displayUploadImageDialog(btnSelected: self.userImgBtn)
    //            case .denied:
    //                print("Error")
    //            default:
    //                break
    //            }
    //        }
    //    }
    
    @IBAction func loadImageButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        //           let photos = PHPhotoLibrary.authorizationStatus()
        //           if photos == .notDetermined {
        //               PHPhotoLibrary.requestAuthorization({status in
        //                   if status == .authorized{
        //                       print("OKAY")
        //                   } else {
        //                       print("NOTOKAY")
        //                   }
        //               })
        //           }
        //           checkLibrary()
        //           checkPermission()
    }
    
    @IBAction func imagePickerBtn(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        //        let photos = PHPhotoLibrary.authorizationStatus()
        //        if photos == .notDetermined {
        //            PHPhotoLibrary.requestAuthorization({status in
        //                if status == .authorized{
        //                    print("OKAY")
        //                } else {
        //                   // self.alertPromptToAllowCameraAccessViaSetting()
        //                    print("NOTOKAY")
        //                }
        //            })
        //        }
        //
        //        checkLibrary()
        //        checkPermission()
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //    func displayUploadImageDialog(btnSelected: UIButton) {
    //        let picker = UIImagePickerController()
    //        picker.delegate = self
    //        picker.allowsEditing = true
    //        let alertController = UIAlertController(title: "", message: "Upload profile photo?", preferredStyle: .actionSheet)
    //        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel action"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
    //            alertController.dismiss(animated: true) {() -> Void in }
    //        })
    //        alertController.addAction(cancelAction)
    //        let library = UIAlertAction(title: NSLocalizedString("Open library", comment: "Open library action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
    //            if UI_USER_INTERFACE_IDIOM() == .pad {
    //                OperationQueue.main.addOperation({() -> Void in
    //                    picker.sourceType = .photoLibrary
    //                    self.present(picker, animated: true) {() -> Void in }
    //                })
    //            }
    //            else {
    //                picker.sourceType = .photoLibrary
    //                self.present(picker, animated: true) {() -> Void in }
    //            }
    //        })
    //
    //        let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: "Open Camera action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
    //            if UI_USER_INTERFACE_IDIOM() == .pad {
    //                OperationQueue.main.addOperation({() -> Void in
    //                    picker.sourceType = .camera
    //                    self.present(picker, animated: true) {() -> Void in }
    //                })
    //            }
    //            else {
    //                print(" Camera Not Available")
    //                 picker.sourceType = .camera
    //                self.present(picker, animated: true) {() -> Void in }
    //            }
    //
    //        })
    //        alertController.addAction(library)
    //        alertController.addAction(camera)
    //
    //        alertController.view.tintColor = .black
    //        present(alertController, animated: true) {() -> Void in }
    //    }
    
    
    
    //    func alertToEncourageCameraAccessInitially() {
    //        let alert = UIAlertController(
    //            title: "IMPORTANT",
    //            message: "Camera access required for capturing photos!",
    //            preferredStyle: UIAlertController.Style.alert
    //        )
    //        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
    //        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
    //            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
    //        }))
    //        present(alert, animated: true, completion: nil)
    //    }
    
    //    func alertPromptToAllowCameraAccessViaSetting() {
    //
    //        let alert = UIAlertController(
    //            title: "IMPORTANT",
    //            message: "Camera access required for capturing photos!",
    //            preferredStyle: UIAlertController.Style.alert
    //        )
    //        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
    //            if AVCaptureDevice.devices(for: AVMediaType.video).count > 0 {
    //                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
    //                    DispatchQueue.main.async() {
    //                        self.checkCamera() } }
    //            }
    //            }
    //        )
    //        present(alert, animated: true, completion: nil)
    //    }
    
    
    @IBAction func userDetailUpdateBtn(_ sender: Any) {
        let fname  = userFirstName.text
        let lname = userLastName.text
        let  countrycodes:String?
        if countrycodevalue! == ""{
            countrycodes = countryCodeBtnOutlet.currentTitle!
        }
        else{
            countrycodes = countrycodevalue
        }
        let mobileNum = userPhoneNum.text
        let email = userEmail.text!
        
        let msg = validtationChecked(name:fname!,lastname:lname!,countrycode:countrycodes!,mobileNum:mobileNum!,email:email)
        
        if msg{
            updateDriverProfile(fname:fname!,lname:lname!,countrycode:countrycodes!,mobileNum:mobileNum!,email:email,image:profileImage! as String)
        }
    }
    
    @IBAction func countryCodeBtnPressed(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CountryCodeViewController") as! CountryCodeViewController
        vc.countrycodeindex = (sender as AnyObject).tag
        vc.delegate = self
        // self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func  validtationChecked(name:String,lastname:String, countrycode:String,mobileNum:String,email:String) -> Bool{
        if (name == ""){
            alert(massage:" Please Insert  First Name")
            return false
        }
        if (lastname == ""){
            alert(massage:" Please Insert Last Name")
            return false
        }
        if (email == ""){
            alert(massage:"Please Insert Email")
            return false
        }
        if email != ""{
            if (!email.isValidEmail){
                alert(massage:" Please Insert Valid Email")
                return false
            }
        }
        if (countrycode == ""){
            alert(massage:" Please Insert Country Code")
            return false
        }
        if (mobileNum == ""){
            alert(massage: " Please Insert Mobile Number")
            return false
        }
        if mobileNum != ""{
            if (!mobileNum.isValidPhone){
                alert(massage:" Please Insert Valid Mobile Number")
                return false
            }
        }
        
        return true
    }
    func alert(massage:String){
        let alert = UIAlertController(title: "", message:massage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
    //  Profile UpDate Api Hit
    func updateDriverProfile(fname:String,lname:String,countrycode:String,mobileNum:String!,email:String,image:String)
    {
        if(!Connectivity.isConnectedToInternet){
            UniversalTaskExecuter.showOKAlertWith(message: "Internet is not connected", title: "Error", self)
            return
        }
        self.loader()
        let myUrl = NSURL(string: "http://fd.yiipro.com/en/api/v1/driver/update");
        
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        let param :[String: Any] = [
            "id":drive_Id!,
            "first_name":fname,
            "last_name":lname,
            "countrycode":countrycode,
            "contact_number":mobileNum!,
            "email":email,
        ]
       // print("param",param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let imageData = userImg.image!.jpegData(compressionQuality: 0.0)
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "image", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                self.dismiss(animated: false, completion: nil)
                
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                if (json!["success"]! as AnyObject).boolValue == true {
                    // self.dismiss(animated: false, completion: nil)
                    var data = json!["data"]
                    let dictionary = data as? [String: Any]
                    //UserDefaults.standard.set(dictionary, forKey: "data")
                    let profileImage :String = dictionary?["profile_image"] as! String
                    let firstName : String = dictionary?["first_name"] as! String
                    let lastName : String = dictionary?["last_name"] as! String
                    let userEmail : String = dictionary?["email"] as! String
                    let phoneNum: String = dictionary?["contact_number"] as! String
                    let countrycode:String = dictionary?["countrycode"] as! String
                    
                    UserDefaultUtils.setUserName(username: firstName+" "+lastName)
                    UserDefaultUtils.setFname(fname: firstName)
                    UserDefaultUtils.setLname(lname: lastName)
                    UserDefaultUtils.setUserEmail(useremail: userEmail)
                    UserDefaultUtils.setProfileImageURL(image:profileImage)
                    UserDefaultUtils.setContactNum(contact:phoneNum)
                    UserDefaultUtils.setCountryCode(country:countrycode)
                    
                    
                    DispatchQueue.main.async {
                        let imageString = UserDefaultUtils.getProfileImageURL()
                        
                        if ( imageString != "") {
                            var escapedString = imageString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                            var url:NSURL = NSURL(string: escapedString as! String)!
                            var data = NSData(contentsOf: url as URL)
                            self.userImg.image = UIImage(data: data! as Data)
                        }
                        self.dismiss(animated: false, completion: nil)
                    }
                }
                else {
                    print("json:",json)
                    //self.dismiss(animated: false, completion: nil)
                    
                    DispatchQueue.main.async {
                        self.dismiss(animated: false, completion: nil)
                    }
                    let contact = json!["message"] as! String
                    UniversalTaskExecuter.showOKAlertWith(message: "\(contact)", title: "Error", self)
                }
            }catch
            {
                self.dismiss(animated: false, completion: nil)
                print(error)
            }
        }
        
        task.resume()
        
    }
    
    //   Create Body Profile Update Api
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> Data {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        
        let profileImageName = "profile.jpg"
        let mimetype = "image/jpg"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(profileImageName)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(Data(imageDataKey))
        body.append(Data("\r\n".utf8))
        
        body.append(Data("--\(boundary)--\r\n".utf8))
        //  print("boundary:",boundary)
        return body as Data
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    func driverProfile(){
        drive_Id = UserDefaultUtils.getUserId()
        
        let imageString = UserDefaultUtils.getProfileImageURL()
        if ( imageString != "") {
            var escapedString = imageString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            var url:NSURL = NSURL(string: escapedString as! String)!
            var data = NSData(contentsOf: url as URL)
            if data != nil{
                self.userImg.image = UIImage(data: data! as Data)
            }
        }
        self.userFirstName.text = UserDefaultUtils.getFname()
        
        self.userLastName.text  = UserDefaultUtils.getLname()
        self.userEmail.text  = UserDefaultUtils.getUserEmail()
        countryCodeBtnOutlet.setTitle(UserDefaultUtils.getCountryCode() as? String, for: .normal)
        self.userPhoneNum.text = UserDefaultUtils.getContactNum()
    }
    
    
    
    func loader() {
        let alert = UIAlertController(title:" Update Profile", message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        self.present(alert, animated: true, completion: nil)
    }
}

extension String {
    var isValidEmail: Bool {
        let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
        return testEmail.evaluate(with: self)
    }
    var isValidPhone: Bool {
        let regularExpressionForPhone = "^\\d{10}$"
        let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
        return testPhone.evaluate(with: self)
    }
    var isValidPassword: Bool {
        let charCount = self.characters.count
        if charCount >= 6 && charCount < 20{
            return true
        }
        else{
            return false
        }
        //        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        //        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
}

extension Data {
    mutating func append(string: String) {
        let data = string.data(
            using: String.Encoding.utf8,
            allowLossyConversion: true)
        append(data!)
    }
}
extension ProfileViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        userImg.image = image
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = userPhoneNum.text
        let newLength = text!.characters.count + string.characters.count
        return newLength <= 10
    }
    
}
