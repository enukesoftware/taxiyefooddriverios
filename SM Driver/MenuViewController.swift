//
//  MenuViewController.swift
//  MultiSideBar
//
//  Created by Mac on 20/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate{
    func slideMenuItemSelectedAtIndex(_ index : Int32)
    //func openViewControllerBasedOnIdentifier(_ strIdentifier:String)
}


class MenuViewController: UIViewController {
    

    
    var btnMenu : UIButton!
    var delegate : SlideMenuDelegate?
    
    @IBOutlet weak var sideMenuCloseOverLap: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeSideMenubtnPressed(_ sender: UIButton) {
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if (self.delegate != nil){
            var index = Int32(sender.tag)
            if (sender == self.sideMenuCloseOverLap){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
        self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor  = UIColor.clear
        }) { (finished) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    

    @IBAction func menuBtnPressed(_ sender: UIButton) {
        let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let DVC = mainStoryBoard.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
        self.navigationController?.pushViewController(DVC, animated: true)
    }
    
    @IBAction func profileBtnPressed(_ sender: UIButton) {
        let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let DVC = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(DVC, animated: true)
    }
  
    

    @IBAction func logoutBtnPressed(_ sender: UIButton) {
        let alertController = UIAlertController(title: "LogOut", message: "Are you Sure LogOut", preferredStyle: UIAlertController.Style.alert)
            
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
           
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in

            print("You've pressed cancel");
        }
        alertController.addAction(action1)
        alertController.addAction(action2)

        
        self.present(alertController, animated: true, completion: nil)
    }
}
