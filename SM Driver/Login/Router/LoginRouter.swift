//
//  LoginRouter.swift
//  SM Driver
//
//  Created by mac-2 on 31/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit

class LoginRouter : LoginPresenterToRouterProtocol {
    
    
    static func createModule() -> LoginViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let presenter: LoginPresenter = LoginPresenter()
        let interactor: LoginInterector = LoginInterector()
        let router:LoginRouter = LoginRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    func pushToNext(navigationConroller: UINavigationController) {
//        UniversalTaskExecuter.showOKAlertWith(message: "Moved To Next", title: "Error", onViewController: navigationConroller)
        
        let dashboard = DashoardRouter.createModule()
        let navigationController = UINavigationController(rootViewController: dashboard)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController

      //  navigationConroller.performSegue(withIdentifier: "ShowDashboard", sender: nil)
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
}
