//
//  LoginPresenterToViewProtocol.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation

protocol LoginPresnterToViewProtocol: class {
    func LoginSucess()
    func LoginError(error:String)
}
