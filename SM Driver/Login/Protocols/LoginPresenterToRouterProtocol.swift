//
//  LoginPresenterToRouterProtocol.swift
//  SM Driver
//
//  Created by mac-2 on 31/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit

protocol LoginPresenterToRouterProtocol : class {
    
    static func createModule()-> LoginViewController
    
    func pushToNext(navigationConroller:UINavigationController)
}
