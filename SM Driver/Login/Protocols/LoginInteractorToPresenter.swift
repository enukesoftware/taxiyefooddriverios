//
//  LoginInteractorToPresenter.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation

protocol LoginInteractorToPresenter:class {
    func loginSucess()
    func loginFail(error:String)
}
