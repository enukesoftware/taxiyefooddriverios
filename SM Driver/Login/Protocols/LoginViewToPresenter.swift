//
//  LoginViewToPresenter.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewToPresenter : class {
    func makeLogin(email:String?,password:String?)
    func showNextScreesn(navgationController : UINavigationController)
}
