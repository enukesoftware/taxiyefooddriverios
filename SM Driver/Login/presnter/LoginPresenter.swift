//
//  LoginPresenter.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import UIKit

class LoginPresenter : LoginInteractorToPresenter , LoginViewToPresenter {
   
    
    
    var view: LoginPresnterToViewProtocol?
    var interactor: LoginPresenterToInteractorProtocol?
    var router : LoginPresenterToRouterProtocol?
    
    func loginSucess() {
        view?.LoginSucess()
    }
    
    func loginFail(error: String) {
        view?.LoginError(error: error)
    }
    
    func showNextScreesn(navgationController: UINavigationController) {
        router?.pushToNext(navigationConroller: navgationController)
    }
    
    func makeLogin(email: String?, password: String?) {
        interactor?.doLoginWithServer(email: email, password: password)
        }
    
    
    
}
