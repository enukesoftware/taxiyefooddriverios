//
//  LoginInterector.swift
//  SM Driver
//
//  Created by mac-2 on 30/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class LoginInterector : LoginPresenterToInteractorProtocol{
    
    var presenter: LoginInteractorToPresenter?
    
    func doLoginWithServer(email: String?, password: String?) {
        if email == nil || password == nil{
            presenter?.loginFail(error: "Please Enter Email and Password")
            return
        }
        if(!UniversalTaskExecuter.isValidEmailAddress(emailAddressString: email!)){
            presenter?.loginFail(error: "Please Enter Valid email address")
            return
        }
        
        if(!Connectivity.isConnectedToInternet){
            presenter?.loginFail(error: "Internet is not connected")
            return
        }
        
        let inputDic:[String:Any] = ["email":email!,"password":password!]
        
        ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.LOGIN_METHOD,headerString: "",  onCompletion: {
            (json: JSON, err: NSError?) in
            if err != nil{
                self.presenter?.loginFail(error: err.debugDescription)
            }
            else{
              //  print(json.stringValue)
                if json["success"].boolValue == true {
                    self.presenter?.loginSucess()
                    UserDefaultUtils.setLoggedIn(isLogin: true)
                    var data = json["data"].dictionaryObject
                   // print("data:",  data)

                   // UserDefaults.standard.set(data, forKey: "data")
                    
                    let user :String = data?["id"].unsafelyUnwrapped as! String
                    let firstName : String = data?["first_name"] as! String
                    let lastName : String = data?["last_name"] as! String
                    let userEmail : String = data?["email"] as! String
                    UserDefaultUtils.setUserId(userId: user)
                    UserDefaultUtils.setUserName(username: firstName+" "+lastName)
                    UserDefaultUtils.setUserEmail(useremail: userEmail)
                    
                    UserDefaultUtils.setFname(fname: firstName)
                    
                    UserDefaultUtils.setLname(lname: lastName)
                    
                    let profileImage :String = data?["profile_image"] as! String
                    
                    UserDefaultUtils.setProfileImageURL(image:profileImage)
                    
                    let phoneNum: String = data?["contact_number"] as! String
                    let countrycode:String = data?["countrycode"] as! String
                    
                    UserDefaultUtils.setContactNum(contact:phoneNum)
                    
                    UserDefaultUtils.setCountryCode(country:countrycode)
//                    print("Driver Id:",UserDefaultUtils.getUserId())
                    
                }
                else{
                    let message = json["message"].stringValue
                    self.presenter?.loginFail(error: message)
                }
            }
        })
    }
    
    
    
   
}
