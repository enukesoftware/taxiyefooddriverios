//
//  LoginViewController.swift
//  SM Driver
//
//  Created by mac-2 on 29/05/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var loginButtonView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var progressHUD: MBProgressHUD!
    
    var presenter: LoginViewToPresenter?
    
    @IBOutlet weak var loginBtnImg: UIButton!
    var drivetoken:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        loginBtnImg.imageEdgeInsets = UIEdgeInsets(top:5, left:200, bottom: 5, right: 20)

        emailTextField.layer.borderColor = UIColor.white.cgColor
        passwordTextField.layer.borderColor = UIColor.white.cgColor
        
       emailTextField.setLeftPaddingPoints(10)
       passwordTextField.setLeftPaddingPoints(10)
        
        loginButtonView.layer.cornerRadius = 25
        
        
//        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(LoginViewController.handleLoginTap(recognizer:)))
//        gestureRecognizer.numberOfTapsRequired = 1
//        self.loginView.isUserInteractionEnabled = true
//        
//        self.loginView.addGestureRecognizer(gestureRecognizer)
        
        emailTextField.text = "ak47@gmail.com"
        passwordTextField.text = "1234567"
    }
    
    
    @IBAction func loginbtnPressed(_ sender: Any) {
        self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)
        presenter?.makeLogin(email: emailTextField.text, password: passwordTextField.text)
    }
    
    @IBAction func registerBtnPressed(_ sender: Any) {
        let register = RegisterRouter.createModule()
        self.navigationController?.pushViewController(register, animated: true)

//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController;
    }
//        @objc func handleLoginTap(recognizer: UITapGestureRecognizer) {
//        self.progressHUD = MBProgressHUD.showAdded(to: Constants.appDelegateConstant.appDelegate.window! , animated: true)
//        presenter?.makeLogin(email: emailTextField.text, password: passwordTextField.text)
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController : LoginPresnterToViewProtocol {
    
    func LoginSucess() {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            UniversalTaskExecuter.showOKAlertWith(message: "Login Sucess", title: "Sucess", self)
            let app = UIApplication.shared.delegate as! AppDelegate
            let  inputDic : [String:String] = [
                "id":UserDefaultUtils.getUserId(),
                "device_token":app.Drivetoken!
            ]
            print("inputDic:",inputDic)
            
            ApiHandler.sharedInstance.callAPIWith(dictionary: inputDic, url: Constants.BASE_URL + Constants.API_VERSION+Constants.Device_Token_METHOD,headerString: "",  onCompletion: {
                    (json: JSON, err: NSError?) in
                    if err != nil{

                    }
                    else{
                        print("Json:",json)
                        if json["success"].boolValue == true {

                        }
                        else{
                            let message = json["message"].stringValue
                              print("message:",message)
                        }
                    }
                })

            self.presenter?.showNextScreesn(navgationController: self.navigationController!)
        }
    }
    
    func LoginError(error: String) {
        DispatchQueue.main.async {
            self.progressHUD.hide(animated: true)
            UniversalTaskExecuter.showOKAlertWith(message: error, title: "Error", self)
        }
    }
    
}
