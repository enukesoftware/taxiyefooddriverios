//
//  UserObject.swift
//  SM Driver
//
//  Created by mac-2 on 03/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

import Foundation

struct User : Codable  {
    let createdAt: String
    let profileImage: String
    let deletedAt, contactNumber, lastName, countrycode: String
    let newsletter, roleID, status, updatedAt: String
    let deviceToken, loginType, lastLong, lastLat: String
    let token, firstName: String
    let verified: String
    let email, id: String

    
    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case profileImage = "profile_image"
        case deletedAt = "deleted_at"
        case contactNumber = "contact_number"
        case lastName = "last_name"
        case countrycode, newsletter
        case roleID = "role_id"
        case status
        case updatedAt = "updated_at"
        case deviceToken = "device_token"
        case loginType = "login_type"
        case lastLong = "last_long"
        case lastLat = "last_lat"
        case token
        case firstName = "first_name"
        case verified, email, id
    }
}
