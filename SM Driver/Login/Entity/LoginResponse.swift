//
//  LoginResponse.swift
//  SM Driver
//
//  Created by mac-2 on 03/06/19.
//  Copyright © 2019 mac-2. All rights reserved.
//

struct LoginResponse : Codable {
        let token: String
        let success: Bool
        let message: String
        let data: User
    }

